package bricksBreaker;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import bricksBreaker.gameParts.collidableParts.Block;
import bricksBreaker.gameParts.collidableParts.Block.SHAPES;

public class FileHelper {

	public static boolean isFileExists(String fileName) {
		File f = new File(fileName);
		return f.exists();
	}

	public static int[][] getBricksFromFile(String fileName) {
		int[][] arr = null;
		int count = 0;
		String last = "";
		try {
			FileReader in = new FileReader(fileName);
			arr = new int[Board.bricksInBoard][Board.bricksInBoard];
			int c;
			while ((c = in.read()) != ',' && c != -1) {
				if (c == '\r' || c == '\n' || c == ' ' || c == '\t' || c == ',') {
					if (last != "") {
						// count -= last.length();
						arr[count % Board.bricksInBoard][count / Board.bricksInBoard] = Integer.parseInt(last);
						last = "";
						count++;
					}
				} else if (c == '*') {
					arr[count % Board.bricksInBoard][count / Board.bricksInBoard] = 0;
					count++;
				} else {
					last += (char) (c);
				}
			}
			if (last != "") {
				arr[count % Board.bricksInBoard][count / Board.bricksInBoard] = Integer.parseInt(last);
			}
			in.close();
			in = null;
		} catch (IOException e) {
		}
		return arr;
	}

	public static ArrayList<Block> getBlocksFromFile(String fileName, JPanel panel) {
		ArrayList<Block> blocks = new ArrayList<>();
		String tmp = "";
		try {
			FileReader in = new FileReader(fileName);
			int c;
			while ((c = in.read()) != ',' && c != -1);
			
			while ((c = in.read()) != -1) {
				if (c == '\r' || c == '\n' || c == '\t') {
					//shape:X:Y:width:height:angle \ CUSTOM:moveX:moveY:pointX:pointY:pointX:pointY:...
					if(!tmp.equals(""))
					{
						String[] vals = tmp.split(":");
						SHAPES s = Enum.valueOf(SHAPES.class, vals[0]);
						Block b = new Block(panel, s);
						int posX = Integer.parseInt(vals[1]);
						int posY = Integer.parseInt(vals[2]);
						b.setPoint(posX, posY);
						if(s != SHAPES.CUSTOM)
						{
							int width = Integer.parseInt(vals[3]);
							int height = Integer.parseInt(vals[4]);
							b.setSize(width, height);
							int angle = Integer.parseInt(vals[5]);
							b.setAngle(angle);
						}
						else
						{
							int size = (vals.length - 3) / 2;
							for(int i = 0; i < size; i++)
							{
								int x = Integer.parseInt(vals[2 * i + 3]);
								int y = Integer.parseInt(vals[2 * i + 4]);
								b.addPoint(x, y);
							}
						}
						blocks.add(b);
						tmp = "";
					}
				} else {
					tmp += (char) (c);
				}
			}
			
			in.close();
			in = null;
		} catch (IOException e) {
		}
		
		return blocks;
	}

	public static void setFileFromBoard(int[][] board, ArrayList<Block> blocks, String fileName) {
		File f = new File(fileName.substring(0, fileName.lastIndexOf("/")));
		if (!f.exists())
			f.mkdirs();
		int count = 1;
		f = new File(fileName);
		String name = fileName;
		while (f.exists()) {
			count++;
			name = fileName.substring(0, fileName.lastIndexOf(".")) + count
					+ fileName.substring(fileName.lastIndexOf("."));
			f = new File(name);
		}
		try {
			FileWriter out = new FileWriter(name);
			for (int i = 0; i < Board.bricksInBoard * Board.bricksInBoard; i++) {
				String t = board[i % Board.bricksInBoard][i / Board.bricksInBoard] + "";
				if (t.equals("0"))
					t = "*";
				char[] tArr = t.toCharArray();
				if (i % Board.bricksInBoard == 0)
					out.append('\n');
				else
					out.append('\t');
				for (int j = 0; j < t.length(); j++) {
					out.append(tArr[j]);
				}
			}
			if(blocks.size() > 0) out.append(",\n");
			for(Block b : blocks)
			{
				out.append(b.toString() + "\n");
			}
			
			out.close();

		} catch (IOException e) {
		}
	}

	public static void saveGame(String fileName, int[][] board, ArrayList<Block> blocks, ArrayList<String> data) {
		int count = 0;
		fileName = "savedGames/" + fileName;
		File f = new File(fileName.substring(0, fileName.lastIndexOf("/")));
		if (!f.exists())
			f.mkdirs();

		String dataFileName = fileName + ".gameData";
		f = new File(dataFileName);
		while (f.exists()) {
			count++;
			dataFileName = fileName + count + ".gameData";
			f = new File(dataFileName);
		}
		setFileFromBoard(board, blocks, fileName + ".lvl");
		try {
			FileWriter out = new FileWriter(dataFileName);
			for (String line : data) {
				out.append(line + "\n");
			}
			out.close();
		} catch (IOException e) {
		}

	}

	public static ArrayList<String> loadGameData(String fileName) {
		ArrayList<String> data = new ArrayList<>();
		try {
			FileReader in = new FileReader(fileName);
			String tmp = "";
			int c;
			while ((c = in.read()) != -1) {
				if (c == '\r' || c == '\n' || c == '\t') {
					if (!tmp.equals("")) {
						data.add(tmp);
						tmp = "";
					}
				} else {
					tmp += (char) (c);
				}
			}
			in.close();
			in = null;
		} catch (IOException e) {
		}
		return data;
	}

	public static String[] getLevelNames(String path) {
		ArrayList<String> lvls = new ArrayList<>();
		File dir = new File(path);
		for (File file : dir.listFiles()) {
			String name = file.getName();
			if (name.contains(".lvl"))
				lvls.add(name.substring(0, name.lastIndexOf(".")));
		}
		String[] arr = new String[lvls.size()];
		for (int i = 0; i < lvls.size(); i++) {
			arr[i] = lvls.get(i);
		}
		return arr;
	}
	
	public static void addToScoreBoard(String name, boolean hasCheated, int level, int timePassed) {
		String path = "others/scoreBoard.data";
		try {
			FileWriter out = new FileWriter(path);
			out.append(name + ":" + hasCheated + ":" + level + ":" + timePassed);	
			out.close();

		} catch (IOException e) {
		}
	}
	
	public static ArrayList<String> getScoreBoardData() {
		ArrayList<String> data = new ArrayList<>();
		String path = "others/scoreBoard.data";
		FileReader in;
		try {
			in = new FileReader(path);
			String tmp = "";
			int c;
			while ((c = in.read()) != -1) {
				if (c == '\r' || c == '\n' || c == '\t') {
					//name:hasCheated:level:timePassed
					if(!tmp.equals(""))
					{
						data.add(tmp);
						tmp = "";
					}
				} else {
					tmp += (char) (c);
				}
			}
			
			in.close();
		} catch (IOException e) {
		}
		in = null;
		return null;
	}
}
