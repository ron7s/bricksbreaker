package bricksBreaker;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.font.GlyphVector;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import bricksBreaker.gameParts.*;
import bricksBreaker.gameParts.collidableParts.Ball;
import bricksBreaker.gameParts.collidableParts.Block;
import bricksBreaker.gameParts.collidableParts.Brick;
import bricksBreaker.gameParts.collidableParts.CreatorBrick;
import bricksBreaker.gameParts.collidableParts.Paddle;
import bricksBreaker.gameParts.collidableParts.PowerUp;
import bricksBreaker.gameParts.collidableParts.Shoot;
import bricksBreaker.gameParts.collidableParts.Wall;
import bricksBreaker.gameParts.collidableParts.Block.SHAPES;
import bricksBreaker.gameParts.invisibleParts.GameActions;
import bricksBreaker.gameParts.killableParts.ComponentOrganizer;
import bricksBreaker.gameParts.killableParts.GameButton;
import bricksBreaker.gameParts.killableParts.GameTextBox;
import bricksBreaker.gameParts.killableParts.KillablePart;

public class Board extends JPanel {
	public static final int bricksInBoard = 20;
	HashMap<String, String> extraInfo = new HashMap<>();
	String keyPresses = "", levelName = "", screenMode;
	ArrayList<GamePart> gameParts;
	ArrayList<GamePart> changingParts;
	ArrayList<GamePart> pauseMenuParts;
	ArrayList<Brick> bricks;
	ArrayList<Effect> effects;
	Image[][] bricksImgs;
	Timer time = new Timer(5, new al());
	public static boolean isPaused = false, isLeftMouseButtonPressed = false, reversedPaddle = false,
			isPaddleSuspended = false, hasFinished = false, isEditMode = false;
	boolean modeChanged = false, gameStarted = false;
	public static int difficulty = 0, level = 1, lifes = 4, ballHitDelay = 0, timePassed = 0, maxNumberOfImages = 8;
	int reverseChangedTime = 0;
	int imgNumber = 0;
	static int mouseX, mouseY;
	static JPanel panel;
	static Bar lifeBar;

	public Board() {
		time.start();
		addMouseMotionListener(new mml());
		addMouseListener(new ml());
		panel = this;
		initPauseParts();
		cleanAll();
		openingScreen();
	}

	public void initPauseParts() {
		pauseMenuParts = new ArrayList<>();
		String[] btns = { "Resume Game", "Save Game", "Return To Main Menu", "Exit Game" };
		ArrayList<GamePart> prts = new ArrayList<>();
		for (String str : btns) {
			GameButton btn = new GameButton(panel, str, new openningScreenButtons());
			btn.setSize(140, 8);
			prts.add(btn);
			btn.start();
			pauseMenuParts.add(btn);
		}
		ComponentOrganizer cmp = new ComponentOrganizer(panel, prts, ComponentOrganizer.Orientation.VERTICAL, 30, 0, 50,
				ComponentOrganizer.Alingment.CENTER, ComponentOrganizer.Alingment.CENTER, false);
		cmp.start();
		pauseMenuParts.add(cmp);
	}

	public void switchScrenMode(String mode) {
		cleanAll();
		screenMode = mode;
		modeChanged = true;
	}
	
	public void scoresScreen() {
		ArrayList<String> data = FileHelper.getScoreBoardData(), arrangedData = new ArrayList<>();
		for (String string : data) {
			
		}
	}

	public void restartGame() {
		Paddle.cheatMode = false;
		GameActions.isCheatsMode = false;
		Brick.isFireMode = false;
		level = 1;
		lifes = 4;
		startLevel();
	}

	private void startLevel() {
		gameStarted = true;
		generateGameParts();
		generateImage();
		generateBlocks();
		generateBricks();
	}

	private void levelCreator() {
		generateImage();
		isEditMode = true;
		lifes = 4;
		for (int x = 0; x < bricksInBoard; x++) {
			for (int y = 0; y < bricksInBoard; y++) {
				CreatorBrick b = new CreatorBrick(this, 0, x, y, bricksImgs[x][y]);
				b.start();
				bricks.add(b);
			}
		}

		ArrayList<GamePart> prts = new ArrayList<>();
		GameTextBox tb = new GameTextBox(panel, "level name:", false);
		prts.add(tb);
		tb = new GameTextBox(panel, new textFieldsListener());
		tb.setWidth(100);
		tb.setName("creatorLvlName");
		prts.add(tb);
		ComponentOrganizer cmp = new ComponentOrganizer(panel, prts, ComponentOrganizer.Orientation.HORIZONTAL, 10, 0,
				20, ComponentOrganizer.Alingment.CENTER, ComponentOrganizer.Alingment.NORMAL, true);
		addLongTermGamePart(cmp);

		prts = new ArrayList<>();
		tb = new GameTextBox(panel, "brick streanth:", false);
		prts.add(tb);
		tb = new GameTextBox(panel, new textFieldsListener());
		tb.setWidth(20);
		tb.setName("creatorBrickStrength");
		prts.add(tb);
		cmp = new ComponentOrganizer(panel, prts, ComponentOrganizer.Orientation.HORIZONTAL, 10, 2, 2,
				ComponentOrganizer.Alingment.NORMAL, ComponentOrganizer.Alingment.OPOSSITE, true);
		addLongTermGamePart(cmp);

		prts = new ArrayList<>();
		GameButton btn = new GameButton(panel, "Return To Main Menu", new creatorButtons());
		prts.add(btn);
		btn = new GameButton(panel, "Save To File", new creatorButtons());
		prts.add(btn);

		cmp = new ComponentOrganizer(panel, prts, ComponentOrganizer.Orientation.VERTICAL, 20, 0, 80,
				ComponentOrganizer.Alingment.CENTER, ComponentOrganizer.Alingment.OPOSSITE, true);
		addLongTermGamePart(cmp);
	}

	@SuppressWarnings("unchecked")
	private void killObjects() {
		if (gameParts == null)
			return;
		ArrayList<GamePart> tmp;
		synchronized (gameParts) {
			tmp = (ArrayList<GamePart>) gameParts.clone();
		}
		for (GamePart prt : tmp) {
			if (prt instanceof KillablePart) {
				prt.kill();
			}
		}
	}

	private void openingScreen() {
		isEditMode = true;
		String[] btns = { "Start Game", "Load Game", "Level Creator", "Play Custom Level", "Instructions", "Exit" };
		ArrayList<GamePart> prts = new ArrayList<>();
		for (String str : btns) {
			GameButton btn = new GameButton(panel, str, new openningScreenButtons());
			btn.setSize(110, 8);
			prts.add(btn);
		}
		ComponentOrganizer cmp = new ComponentOrganizer(panel, prts, ComponentOrganizer.Orientation.VERTICAL, 30, 0, 0,
				ComponentOrganizer.Alingment.CENTER, ComponentOrganizer.Alingment.CENTER, true);
		addLongTermGamePart(cmp);
	}

	private void cleanAll() {
		killObjects();
		gameParts = new ArrayList<>();
		changingParts = new ArrayList<>();
		bricks = new ArrayList<>();
		effects = new ArrayList<>();
		isPaused = false;
		gameStarted = false;
		isEditMode = false;
		isPaddleSuspended = false;
	}

	public void generateImage() {
		if (imgNumber == 0)
			imgNumber = (int) (Math.random() * 9) + 1;
		File file = new File("imgs/bg" + imgNumber + ".jpg");
		imgNumber++;
		if (imgNumber > maxNumberOfImages)
			imgNumber = 1;
		BufferedImage image = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			image = ImageIO.read(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
		bricksImgs = ImageHelper.SplitImage(image, bricksInBoard, bricksInBoard);
	}

	private void generateGameParts() {
		int ballsCount = 1;
		if (level > 4)
			ballsCount = 3;
		else if (level > 3)
			ballsCount = 2;
		for (int i = 0; i < ballsCount; i++) {
			Ball b = new Ball(this, true);
			b.setBaseSpeed(b.getBaseSpeed() + (ballsCount - 1) * 2);
			b.start();
			gameParts.add(b);
		}
		GameActions ga = new GameActions(this);
		ga.start();
		gameParts.add(ga);
		for (int i = 0; i < Math.pow(2, difficulty); i++) {
			Paddle p = new Paddle(this, i);
			p.start();
			gameParts.add(p);
		}
		generateNotSavedGameParts();
	}

	private void generateNotSavedGameParts() {
		lifeBar = new Bar(this, lifes, 2, "imgs/heart.png");
		lifeBar.setPoint(40, 8);
		lifeBar.start();
		gameParts.add(lifeBar);
	}

	public synchronized void addShortTermGamePart(GamePart gp) {
		gp.start();
		changingParts.add(gp);
	}

	public synchronized void addLongTermGamePart(GamePart gp) {
		gp.start();
		gameParts.add(gp);
	}

	public void addEffect(Effect ef) {
		synchronized (effects) {
			ef.start();
			effects.add(ef);
		}
	}

	public void removeEffect(Effect ef) {
		synchronized (effects) {
			effects.remove(ef);
		}

	}

	private void generateBricks() {// TODO create randomly generated levels
		int[][] arr = new int[bricksInBoard][bricksInBoard];
		if (level > 0) {
			if (FileHelper.isFileExists(getFullLevelName("level" + level, "lvls/")))
				arr = FileHelper.getBricksFromFile(getFullLevelName("level" + level, "lvls/"));
			else {
				switchScrenMode("opening");
				Sounds.playSound("gameOverGood2");
				JOptionPane.showMessageDialog(panel, "Game over, you won!!!");
				return;
			}
		} else if (level < 0) {// randomly generated levels

		} else if (level == 0) {// customly created levels
			if (FileHelper.isFileExists(getFullLevelName(levelName, "customLvls/")))
				arr = FileHelper.getBricksFromFile(getFullLevelName(levelName, "customLvls/"));
		}
		generateBricks(arr);
	}

	private void generateBricks(int[][] arr) {
		for (int x = 0; x < arr.length; x++) {
			for (int y = 0; y < arr[x].length; y++) {
				Brick b = new Brick(this, arr[x][y], x, y, bricksImgs[x][y]);
				b.start();
				bricks.add(b);
			}
		}
	}

	private void generateBlocks() {
		ArrayList<Block> blocks = new ArrayList<>();
		if (level > 0 && FileHelper.isFileExists(getFullLevelName("level" + level, "lvls/"))) {
			blocks = FileHelper.getBlocksFromFile(getFullLevelName("level" + level, "lvls/"), panel);
		} else if (level < 0) {// randomly generated levels

		} else if (level == 0 && FileHelper.isFileExists(getFullLevelName(levelName, "customLvls/"))) {
			// customly created levels
			blocks = FileHelper.getBlocksFromFile(getFullLevelName(levelName, "customLvls/"), panel);
		}
		for (Block b : blocks) {
			addLongTermGamePart(b);
		}
	}

	private String getFullLevelName(String fileName, String path) {
		return (path + fileName + ".lvl");
	}

	@SuppressWarnings("unchecked")
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (lifeBar != null)
			lifeBar.setAmount(lifes);
		g.setFont(new Font("ariel", 0, 12));
		boolean isDone = true;
		ArrayList<GamePart> tmp;
		synchronized (gameParts) {
			tmp = (ArrayList<GamePart>) bricks.clone();
		}
		for (GamePart b : tmp) {
			if (b.isAlive()) {
				b.draw(g);
				isDone = false;
			}
		}
		if (isDone && gameStarted && lifes > 0) {
			level++;
			lifes++;
			if (level > 1) {
				startLevel();
			} else {
				switchScrenMode("opening");
				Sounds.playSound("gameOverGood1");
				JOptionPane.showMessageDialog(panel, "custom level ended");
			}
		} else if (lifes < 1) {
			lifes = 4;
			switchScrenMode("opening");
			Sounds.playSound("gameOverBad");
			JOptionPane.showMessageDialog(panel, "Game over ):");
		}

		synchronized (gameParts) {
			tmp = (ArrayList<GamePart>) gameParts.clone();
		}
		for (GamePart gp : tmp) {
			if (gp.isAlive())
				gp.draw(g);
		}

		synchronized (changingParts) {
			tmp = (ArrayList<GamePart>) changingParts.clone();
		}
		for (GamePart gp : tmp) {
			if (gp.isAlive())
				gp.draw(g);
		}

		synchronized (effects) {
			tmp = (ArrayList<GamePart>) effects.clone();
		}
		for (GamePart ef : tmp) {
			if (ef.isAlive())
				ef.draw(g);
		}
		g.setColor(Color.red);
		if (gameStarted) {
			g.drawString("lifes: ", 5, 20);
			g.setFont(new Font("ariel", 0, 25));
			g.setColor(Color.BLUE);
			String text = "level: " + ((level > 0) ? level : ((level < 0) ? "random" + (level * -1) : levelName));
			int txtWidth = getTextWidth(text, g);
			g.drawString(text, (getWidth() / 2) - (txtWidth / 2), 30);
		}

		if (gameStarted && isPaused) {
			g.setColor(new Color(0, 0, 0, 180));
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			g.setColor(Color.red);
			g.setFont(new Font("ariel", 2, 60));
			String txt = "Game Paused";
			Rectangle2D bounds = getTextBounds(txt, 0, txt.length(), g);
			int strWidth = (int) bounds.getWidth();
			g.drawString(txt, this.getWidth() / 2 - strWidth / 2, 200);
			g.setFont(new Font("ariel", 0, 12));
			synchronized (pauseMenuParts) {
				tmp = (ArrayList<GamePart>) pauseMenuParts.clone();
			}
			for (GamePart gp : tmp) {
				if (gp.isAlive())
					gp.draw(g);
			}
		}
	}

	private Rectangle2D getTextBounds(String txt, int startPos, int endPos, Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		if (g == null)
			return new Rectangle(0, 0, 0, 0);
		Font f = g.getFont();
		String tmp;
		try {
			tmp = txt.substring(startPos, endPos);
		} catch (Exception e) {
			tmp = "";
		}
		GlyphVector gv = f.layoutGlyphVector(g2d.getFontRenderContext(), tmp.toCharArray(), 0, tmp.length(),
				GlyphVector.FLAG_MASK);
		return gv.getVisualBounds();
	}

	private int getTextWidth(String txt, Graphics g) {
		return (int) getTextBounds(txt, 0, txt.length(), g).getWidth();
	}

	@SuppressWarnings("unused")
	private int getTextHeight(String txt, Graphics g) {
		return (int) getTextBounds(txt, 0, txt.length(), g).getHeight();
	}

	public static Color RandomizeColor() {
		return new Color((int) (Math.random() * 256), (int) (Math.random() * 256), (int) (Math.random() * 256));
	}

	public ArrayList<GamePart> getGameParts() {
		return gameParts;
	}

	public ArrayList<GamePart> getShortTermGameParts() {
		return changingParts;
	}

	public ArrayList<Brick> getBricks() {
		return bricks;
	}

	public HashMap<String, String> getExtraInfo() {
		return extraInfo;
	}

	public void hideCursor() {
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
		setCursor(blankCursor);
	}

	public void setNormalCursor() {
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	public void setTextCursor() {
		setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
	}

	public void saveGame(String name) {// TODO add all important things to save
		ArrayList<String> data = new ArrayList<>();

		int[][] arr = new int[bricksInBoard][bricksInBoard];
		for (Brick b : bricks) {
			arr[b.getX()][b.getY()] = b.getBrickStreanth();
		}
		ArrayList<Block> blocks = new ArrayList<>();
		for (GamePart gp : gameParts) {
			if (gp instanceof Block)
				blocks.add((Block) gp);
		}

		data.add("reversedPaddle=" + reversedPaddle);
		data.add("difficulty=" + difficulty);
		data.add("level=" + level);
		data.add("lifes=" + lifes);
		data.add("timePassed=" + timePassed);
		data.add("reverseChangedTime=" + reverseChangedTime);
		data.add("imgNumber=" + imgNumber);
		data.add("levelName=" + levelName);
		for (GamePart prt : changingParts) {// ShortTermGamePart
			if (prt.isAlive() && (prt instanceof PowerUp || prt instanceof Shoot || prt instanceof Wall)) {
				ArrayList<String> tmp = prt.getSaveData();
				for (String info : tmp) {
					data.add(info);
				}
			}
		}
		for (GamePart prt : gameParts) {// LongTermGamePart
			if (prt.isAlive() && (prt instanceof Ball || prt instanceof Gun
					|| prt instanceof GameActions || prt instanceof Paddle)) {
				ArrayList<String> tmp = prt.getSaveData();
				for (String info : tmp) {
					data.add(info);
				}
			}
		}
		FileHelper.saveGame(name, arr, blocks, data);
		JOptionPane.showMessageDialog(panel, "saved...");
	}

	public void loadGame(String fileName) {// TODO continue load game in here
		ArrayList<String> data = FileHelper.loadGameData("savedGames/" + fileName + ".gameData");
		int[][] arr = FileHelper.getBricksFromFile("savedGames/" + fileName + ".lvl");
		gameStarted = true;
		isPaused = true;
		HashMap<String, String> tmp = new HashMap<>();
		String type = "Board";
		for (String line : data) {
			if (!line.contains("}") && !line.contains("{")) {
				tmp.put(line.substring(0, line.indexOf('=')),
						(line.length() > line.indexOf('=') + 1) ? line.substring(line.indexOf('=') + 1) : "");
			} else {
				GamePart gp;
				switch (type) {
				case "Board":
					reversedPaddle = Boolean.parseBoolean(tmp.get("reversedPaddle"));
					difficulty = Integer.parseInt(tmp.get("difficulty"));
					level = Integer.parseInt(tmp.get("level"));
					lifes = Integer.parseInt(tmp.get("lifes"));
					timePassed = Integer.parseInt(tmp.get("timePassed"));
					reverseChangedTime = Integer.parseInt(tmp.get("reverseChangedTime"));
					imgNumber = Integer.parseInt(tmp.get("imgNumber"));
					levelName = tmp.get("levelName");
					break;
				case "Wall":
					gp = new Wall(this);
					if (tmp.size() > 0) {
						gp.loadDataFromSave(tmp);
						addShortTermGamePart(gp);
					}
					break;
				case "Shoot":
					gp = new Shoot(this, 0, 0);
					if (tmp.size() > 0) {
						gp.loadDataFromSave(tmp);
						addShortTermGamePart(gp);
					}
					break;
				case "PowerUp":
					gp = new PowerUp(this, 0, 0, level);
					if (tmp.size() > 0) {
						gp.loadDataFromSave(tmp);
						addShortTermGamePart(gp);
					}
					break;
				case "Ball":
					gp = new Ball(this, false);
					if (tmp.size() > 0) {
						gp.loadDataFromSave(tmp);
						addLongTermGamePart(gp);
					}
					break;
				case "GameActions":
					gp = new GameActions(this);
					if (tmp.size() > 0) {
						gp.loadDataFromSave(tmp);
						addLongTermGamePart(gp);
					}
					break;
				case "Gun":
					gp = new Gun(this, 0);
					if (tmp.size() > 0) {
						gp.loadDataFromSave(tmp);
						addLongTermGamePart(gp);
					}
					break;
				case "Paddle":
					gp = new Paddle(this, 0);
					if (tmp.size() > 0) {
						gp.loadDataFromSave(tmp);
						addLongTermGamePart(gp);
					}
					break;
				}

				tmp = new HashMap<>();
				if (line.contains("{")) {
					type = line.substring(0, line.indexOf("{"));
				}
			}
		}
		generateNotSavedGameParts();
		generateImage();
		generateBlocks();
		generateBricks(arr);
	}

	public static int getMouseX() {
		return mouseX;
	}

	public static int getMouseY() {
		return mouseY;
	}

	class cl extends ComponentAdapter {
		public void componentResized(ComponentEvent e) {
			int size = e.getComponent().getWidth() - 16;
			if (size % 20 != 0 && !isLeftMouseButtonPressed) {
				e.getComponent().setSize(size - (size % 20) + 16, e.getComponent().getHeight());
			}
			for (GamePart gp : gameParts) {
				if (gp instanceof Block) {
					((Block) gp).setPoints();
				}
			}
		}

	}

	class al implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			timePassed += time.getDelay();
			repaint();
			if (ballHitDelay > 0)
				ballHitDelay -= time.getDelay();
			if (reversedPaddle && reverseChangedTime == 0) {
				reverseChangedTime = timePassed;
			} else if (timePassed - reverseChangedTime >= 5000) {
				reversedPaddle = false;
				reverseChangedTime = 0;
			}
			if (modeChanged) {
				modeChanged = false;
				switch (screenMode) {
				case "game":
					restartGame();
					break;
				case "loadGame":
					JComboBox<String> loadOptions = new JComboBox<>(FileHelper.getLevelNames("savedGames/"));
					if (loadOptions.getItemCount() > 0) {
						int loadResult = JOptionPane.showConfirmDialog(panel, loadOptions,
								"choose the level you want to start", JOptionPane.OK_CANCEL_OPTION,
								JOptionPane.PLAIN_MESSAGE);
						if (loadResult == JOptionPane.OK_OPTION) {
							String saveName = (String) loadOptions.getSelectedItem();
							loadGame(saveName);
						} else {
							openingScreen();
						}
					} else {
						JOptionPane.showMessageDialog(panel, "no saved games");
						openingScreen();
					}
					break;
				case "customGame":
					level = 0;
					JComboBox<String> options = new JComboBox<>(FileHelper.getLevelNames("customLvls/"));
					if (options.getItemCount() > 0) {
						int result = JOptionPane.showConfirmDialog(panel, options, "choose the level you want to start",
								JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
						if (result == JOptionPane.OK_OPTION) {
							cleanAll();
							levelName = (String) options.getSelectedItem();
							startLevel();
						} else {
							openingScreen();
						}
					} else {
						JOptionPane.showMessageDialog(panel, "no custom games");
						openingScreen();
					}
					break;
				case "creator":
					levelCreator();
					break;
				case "opening":
				default:
					openingScreen();
					break;
				}
			}
		}
	}

	class mml implements MouseMotionListener {

		@Override
		public void mouseDragged(MouseEvent e) {
			paddleMovement(e);
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			paddleMovement(e);
		}

		public void paddleMovement(MouseEvent e) {// TODO add more mouse modes
			mouseX = e.getX();
			mouseY = e.getY();
			boolean isMouseAboveTextField = false;
			for (GamePart gp : gameParts) {
				if (gp instanceof Paddle && !isPaused) {
					Paddle p = (Paddle) gp;
					int newX = e.getX() - p.getWidth() / 2;
					int newY = e.getY() - p.getHeight() / 2;
					if (reversedPaddle) {
						newX = getWidth() - newX - p.getWidth();
						newY = getHeight() - newY - p.getHeight();
					}
					if (!isPaddleSuspended) {
						if (p.getLocation() < 2)
							gp.setPoint(newX, p.getY());
						else
							gp.setPoint(p.getX(), newY);
					}
				} else if (gp instanceof GameTextBox) {
					if (((GameTextBox) gp).isMouseHovering())
						isMouseAboveTextField = true;
				}
			}
			if (isMouseAboveTextField)
				setTextCursor();
			else
				setNormalCursor();
		}

	}

	class ml implements MouseListener {
		@Override
		public void mouseClicked(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
			isLeftMouseButtonPressed = true;
			for (GamePart gp : gameParts) {
				if (gp instanceof GameTextBox) {
					((GameTextBox) gp).setPressedLocation(e.getX());
				}
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			isLeftMouseButtonPressed = false;

			for (GamePart gp : gameParts) {
				if (gameStarted && !isPaused && gp.isAlive()) {
					if (gp instanceof Ball) {
						((Ball) gp).activate();
					} else if (gp instanceof Gun) {
						((Gun) gp).fire();
					}
				}
			}
		}

	}

	class creatorButtons implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			GameButton btn = (GameButton) e.getSource();
			switch (btn.getText()) {
			case "Save To File":
				cominceSave();
				break;
			case "Return To Main Menu":
				switchScrenMode("opening");
				break;
			}

		}

		public void cominceSave() {
			int[][] arr = new int[bricksInBoard][bricksInBoard];
			for (Brick b : bricks) {
				arr[b.getX()][b.getY()] = b.getBrickStreanth();
			}
			ArrayList<Block> blocks = new ArrayList<>();
			for (GamePart gp : gameParts) {
				if (gp instanceof Block)
					blocks.add((Block) gp);
			}
			if (extraInfo.containsKey("creatorLvlName") && extraInfo.get("creatorLvlName") != "") {
				FileHelper.setFileFromBoard(arr, blocks, "customLvls/" + extraInfo.get("creatorLvlName") + ".lvl");
				JOptionPane.showMessageDialog(panel, "saved...");
				switchScrenMode("opening");
			} else {
				JOptionPane.showMessageDialog(panel, "please enter file name...");
			}
		}

	}

	class openningScreenButtons implements ActionListener {// TODO add instruction
		@Override
		public void actionPerformed(ActionEvent e) {
			GameButton btn = (GameButton) e.getSource();
			switch (btn.getText()) {
			case "Start Game":
				switchScrenMode("game");
				break;
			case "Load Game":
				switchScrenMode("loadGame");
				break;
			case "Level Creator":
				switchScrenMode("creator");
				break;
			case "Play Custom Level":
				switchScrenMode("customGame");
				break;
			case "Instructions":
				JOptionPane.showMessageDialog(panel,
						"The game pretty simple break all the bricks to finish the level.\n"
								+ "feel free to create custom levels. and play levels that others created.\n");
				break;
			case "Exit Game":
				if (!isPaused)
					break;
			case "Exit":
				System.exit(0);
				break;
			case "Resume Game":
				if (isPaused) {
					isPaused = false;
					for (int i = 0; i < gameParts.size(); i++) {
						synchronized (gameParts.get(i)) {
							gameParts.get(i).notify();
						}
					}
					for (int i = 0; i < changingParts.size(); i++) {
						synchronized (changingParts.get(i)) {
							changingParts.get(i).notify();
						}
					}
					for (int i = 0; i < bricks.size(); i++) {
						synchronized (bricks.get(i)) {
							bricks.get(i).notify();
						}
					}
				}
				break;
			case "Save Game":
				if (isPaused) {
					String fileName = JOptionPane.showInputDialog(panel, "enter save name");
					if (fileName == null || fileName.equals("")) {
						JOptionPane.showMessageDialog(panel, "not saved...");
					} else {
						saveGame(fileName);
					}
				}
				break;
			case "Return To Main Menu":
				if (isPaused) {
					switchScrenMode("opening");
				}
				break;
			default:
				System.out.println("the button " + btn.getText() + " not implemented yet");
				break;
			}
		}

	}

	class textFieldsListener implements CaretListener {
		@Override
		public void caretUpdate(CaretEvent e) {
			GameTextBox txtF = (GameTextBox) e.getSource();
			extraInfo.put(txtF.getName(), txtF.getText());
		}

	}

	class kl extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent key) {
			if (!gameStarted) {
				for (GamePart gp : gameParts) {
					if (gp instanceof GameTextBox && gp.isAlive()) {
						((GameTextBox) gp).recieveAction(KeyEvent.getKeyText(key.getKeyCode()));
					}
				}
				if (bricks.size() > 0 && (key.getKeyCode() == KeyEvent.VK_S)
						&& ((key.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
					new creatorButtons().cominceSave();
				} else if (key.getKeyCode() == KeyEvent.VK_ESCAPE) {
					int option = JOptionPane.showConfirmDialog(panel, "do you want to exit without saving changes?",
							"choose an option", JOptionPane.YES_NO_OPTION);
					if (option == 0) {
						switchScrenMode("openning");
					}
				}
			} else if ((key.getKeyCode() == KeyEvent.VK_S) && ((key.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
				isPaused = true;
				String fileName = JOptionPane.showInputDialog(panel, "enter save name");
				if (fileName == null || fileName.equals("")) {
					JOptionPane.showMessageDialog(panel, "not saved...");
				} else {
					saveGame(fileName);
				}
			} else if (key.getKeyCode() == KeyEvent.VK_RIGHT) {
				for(GamePart gp : gameParts) {
					if(gp instanceof Paddle && !isPaused)
					{
						Paddle pdl = (Paddle)gp;
						if(pdl.getLocation() % 2 != 0) continue;
						int x = pdl.getX();
						int y = pdl.getY();
						
						pdl.setPoint(x + 10, y);
						
					}
				}
			} else if (key.getKeyCode() == KeyEvent.VK_LEFT) {
				for(GamePart gp : gameParts) {
					if(gp instanceof Paddle && !isPaused)
					{
						Paddle pdl = (Paddle)gp;
						if(pdl.getLocation() % 2 != 0) continue;
						int x = pdl.getX();
						int y = pdl.getY();
						
						pdl.setPoint(x - 10, y);
						
					}
				}
			} else if (key.getKeyCode() == KeyEvent.VK_UP) {
				for(GamePart gp : gameParts) {
					if(gp instanceof Paddle && !isPaused)
					{
						Paddle pdl = (Paddle)gp;
						if(pdl.getLocation() % 2 != 0) continue;
						int x = pdl.getX();
						int y = pdl.getY();
						
						pdl.setPoint(x, y - 10);
						
					}
				}
			} else if (key.getKeyCode() == KeyEvent.VK_LEFT) {
				for(GamePart gp : gameParts) {
					if(gp instanceof Paddle && !isPaused)
					{
						Paddle pdl = (Paddle)gp;
						if(pdl.getLocation() % 2 != 0) continue;
						int x = pdl.getX();
						int y = pdl.getY();
						
						pdl.setPoint(x, y + 10);
						
					}
				}
			} else if (key.getKeyCode() == KeyEvent.VK_SPACE) {
				for(GamePart gp : gameParts) {
					if(gp instanceof Ball && !isPaused)
					{
						Ball b = (Ball)gp;
						b.activate();
					}
				}
			}
		}

		@Override
		public void keyReleased(KeyEvent key) {
			if ((key.getKeyCode() == KeyEvent.VK_P || key.getKeyCode() == KeyEvent.VK_ESCAPE) && gameStarted) {
				isPaused = !isPaused;
				if (!isPaused) {
					for (int i = 0; i < gameParts.size(); i++) {
						synchronized (gameParts.get(i)) {
							gameParts.get(i).notify();
						}
					}
					for (int i = 0; i < changingParts.size(); i++) {
						synchronized (changingParts.get(i)) {
							changingParts.get(i).notify();
						}
					}
					for (int i = 0; i < bricks.size(); i++) {
						synchronized (bricks.get(i)) {
							bricks.get(i).notify();
						}
					}
				}
			} else if (gameStarted) {
				if (key.getKeyCode() == KeyEvent.VK_S && keyPresses.lastIndexOf('r') == keyPresses.length() - 1) {
					keyPresses = "rs";
				} else {
					keyPresses += (key.getKeyChar() + "").toLowerCase();
					checkForCode();
				}
			} else {
				for (GamePart gp : gameParts) {
					if (gp instanceof GameTextBox && gp.isAlive()) {
						((GameTextBox) gp).recieveReleseAction(KeyEvent.getKeyText(key.getKeyCode()));
					}
				}
			}
		}

		void checkForCode() {
			if (keyPresses.equals("rsguns")) {
				PowerUp.activateGuns(panel, 0);
			} else if (keyPresses.equals("rswall")) {
				PowerUp.activateWall(panel, 0);
			} else if (keyPresses.equals("rslife")) {
				lifes++;
			} else if (keyPresses.equals("rssize")) {
				PowerUp.resizePaddle(1, true);
			} else if (keyPresses.equals("rseball")) {
				PowerUp.addBall(panel);
			} else if (keyPresses.equals("rsfireball")) {
				Brick.isFireMode = true;
				GameActions.isCheatsMode = true;
			} else if (keyPresses.equals("rsallbest")) {
				PowerUp.activateGuns(panel, 0);
				PowerUp.activateWall(panel, 0);
				PowerUp.resizePaddle(1, true);
				Brick.isFireMode = true;
				GameActions.isCheatsMode = true;
			} else if (keyPresses.equals("rsnext")) {
				level++;
				if (level > 1) {
					cleanAll();
					startLevel();
				} else {
					switchScrenMode("opening");
					JOptionPane.showMessageDialog(panel, "custom level ended");
				}

			}
		}

		@Override
		public void keyTyped(KeyEvent key) {

		}

	}

	public static void main(String[] args) {
		JFrame f = new JFrame("Bricks Breaker");
		Board bp = new Board();
		f.add(bp);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(1036, 728);
		f.setVisible(true);
		f.addComponentListener(bp.new cl());
		f.addKeyListener(bp.new kl());
		// bp.hideCursor();
	}
}
