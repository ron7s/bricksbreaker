package bricksBreaker;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class ImageHelper {

	public static Image[][] SplitImage(BufferedImage img, int rows, int cols){
		int width = img.getWidth(null) / rows;
		int height = img.getHeight(null) / cols;
		Image[][] imgs = new Image[rows][cols];
		for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                imgs[row][col] = new BufferedImage(width, height, img.getType());

                Graphics2D gr = ((BufferedImage) imgs[row][col]).createGraphics();
                gr.drawImage(img, 0, 0, width, height,
                		width*row, height*col,
                		width*row  + width,
                		height*col + height, null);
                gr.dispose();
            }
        }
		return imgs;
	}
}
