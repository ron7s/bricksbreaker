package bricksBreaker;

public class Vector {
	double x, y;

	public Vector(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Vector(double angle) {
		this(angle, 1);
	}

	public Vector(double angle, double size) {
		double rads = toRadians(angle);
		x = Math.acos(rads) * size;
		y = Math.asin(rads) * size;
	}

	public double toRadians(double angle) {
		return angle / 180 * Math.PI;
	}

	public double dot(Vector v) {
		return this.x * v.x + this.y * v.y;
	}

	public double getSize() {
		return ExtraMath.getDistance(x, y, 0, 0);
	}
	
	public double calculateAngle(Vector v) {
		return Math.acos(dot(v) / (this.getSize() * v.getSize()));
	}
	
	public Vector scalarMultiplication(double scalar) {
		return new Vector(x * scalar, y * scalar);
	}
	
	public Vector sub(Vector v) {
		return new Vector(this.x - v.x, this.y - v.y);
	}
	
	public double getAngle() {
		return Math.atan(y / x);
	}
	
	public Vector normal() {
		return new Vector(Math.atan(-1 / (y / x)), getSize());
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	

}
