package bricksBreaker.gameParts;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.gameParts.collidableParts.Paddle;
import bricksBreaker.gameParts.collidableParts.Shoot;

public class Gun extends GamePart {
	int pos;//-1 0 1
	boolean sholdDie;
	int timeToLive;
	
	public Gun(JPanel panel, int pos) {
		super(panel);
		this.pos = pos;
		timeToLive = 1;
		sholdDie = false;
	}
	
	public Gun(JPanel panel, int pos, int timeToLive) {
		super(panel);
		this.pos = pos;
		this.timeToLive = timeToLive;
		sholdDie = true;
	}
	
	public void run(){
		while(Board.lifes > 0 && timeToLive > 0 && level == Board.level)
		{
			super.run();
			if(sholdDie)
			{
				timeToLive -= delay;
			}
		}
	}
	
	public void draw(Graphics g) {
		height = 5;
		width = 14;
		for (GamePart gp : ((Board) p).getGameParts()) {
			if (gp instanceof Paddle && ((Paddle) gp).getLocation() == 0) {
				x = gp.getX() + gp.getWidth() / 2 + pos * (gp.getWidth() / 4) - width / 2;
				y = gp.getY() - height;
			}
		}
		g.setColor(Color.black);
		g.fillRect(x, y, width, height);
	}
	
	public void fire() {
		((Board)p).addShortTermGamePart(new Shoot(p, x, y - 5));
	}
	
	public ArrayList<String> getSaveData() {
		ArrayList<String> data = new ArrayList<>();
		data.add("Gun{");
		data.add("pos=" + pos);
		data.add("timeToLive=" + timeToLive);
		data.add("sholdDie=" + sholdDie);
		ArrayList<String> tmp = super.getSaveData();
		for(String info : tmp)
		{
			data.add(info);
		}
		data.add("}");
		return data;
	}
	
	public void loadDataFromSave(HashMap<String, String> data) {
		super.loadDataFromSave(data);
		pos = Integer.parseInt(data.get("pos"));
		timeToLive = Integer.parseInt(data.get("timeToLive"));
		sholdDie = Boolean.parseBoolean(data.get("sholdDie"));
	}

}
