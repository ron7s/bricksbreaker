package bricksBreaker.gameParts;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import bricksBreaker.Board;

public class Bar extends GamePart {
	int amount;
	int space;
	ImageIcon icon = null;
	public Bar(JPanel panel, int amount, int space, String imgLocation) {
		super(panel);
		initVars(amount, space, imgLocation);
	}
	public Bar(JPanel panel, int amount, int space) {
		super(panel);
		initVars(amount, space, null);
	}
	public Bar(JPanel panel, int amount) {
		super(panel);
		initVars(amount, 0, null);
	}
	
	void initVars(int amount, int space, String imgLocation) {
		this.amount = amount;
		this.space = space;
		if(imgLocation != null)
			icon = new ImageIcon(imgLocation);
		height = 40;
		width = 20;
	}
	
	public void run(){
		while(Board.lifes > 0 && level == Board.level) {
			super.run();
		}
	}
	
	public void draw(Graphics g) {
		for(int i = 0; i < amount; i++) {
			if(icon != null)
			{
				Image img = icon.getImage();
				float realativeDifference = (float)img.getWidth(null) / img.getHeight(null);
				height = Math.round(width / realativeDifference );
				g.drawImage(img, x + (width + space) * i, y, width, height, null);
			}
			else
			{
				g.fillRect(x + (width + space) * i , y, width, height);
			}
		}
	}
	
	
	public int getAmount() {
		return amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}

}
