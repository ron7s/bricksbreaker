package bricksBreaker.gameParts.collidableParts;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import bricksBreaker.Board;
import bricksBreaker.Sounds;
import bricksBreaker.gameParts.Effect;
import bricksBreaker.gameParts.GamePart;
import bricksBreaker.gameParts.Gun;

public class PowerUp extends GamePart {
	TYPES type;
	int speed = 5;
	Color color = Color.black;
	String location = "";
	ImageIcon icon = null;

	enum TYPES {
		NONE, extraLife, paddleBig, paddleSmall, ballSpeedFast, ballSpeedSlow, paddleOppositeMouse,
		paddleSticky, fireBall, moreBalls, shoterPaddle, wallBehindPaddle, bomb
	}

	public PowerUp(JPanel panel, int x, int y, int level) {
		super(panel);
		this.level = level;
		type = randomizeType();
		this.x = x;
		this.y = y;
		switch (type) {
		case extraLife:
			location = "imgs/heart.png";
			icon = new ImageIcon(location);
			break;
		case paddleBig:
			location = "imgs/expandPaddle.png";
			icon = new ImageIcon(location);
			break;
		case paddleSmall:
			location = "imgs/shrinkPaddle.png";
			icon = new ImageIcon(location);
			break;
		case ballSpeedFast:
			location = "imgs/fasterBall.png";
			icon = new ImageIcon(location);
			break;
		case ballSpeedSlow:
			location = "imgs/slowerBall.png";
			icon = new ImageIcon(location);
			break;
		case paddleOppositeMouse:
			location = "imgs/reversePaddle.png";
			icon = new ImageIcon(location);
			break;
		case paddleSticky:
			location = "imgs/stickyPaddle.png";
			icon = new ImageIcon(location);
			break;
		case fireBall:
			location = "imgs/fireBall.png";
			icon = new ImageIcon(location);
			break;
		case moreBalls:
			location = "imgs/extraBall.png";
			icon = new ImageIcon(location);
			break;
		case shoterPaddle:
			location = "imgs/gunsPaddle.png";
			icon = new ImageIcon(location);
			break;
		case wallBehindPaddle:
			location = "imgs/wall.png";
			icon = new ImageIcon(location);
			break;
		case bomb:
			location = "imgs/bomb.png";
			icon = new ImageIcon(location);
			break;
		default:
			color = Color.BLACK;
			break;
		}
	}

	public void run() {
		boolean wasAlive = false;
		boolean colided = hasColided();
		while (!colided && type != TYPES.NONE && y < p.getHeight() && level == Board.level) {
			wasAlive = true;
			super.run();
			y += speed;
			colided = hasColided();
		}
		if (wasAlive && colided) {
			switch (type) {
			case extraLife:
				Board.lifes++;
				Sounds.playSound("gainHeart");
				break;
			case paddleBig:
				resizePaddle(1, false);
				Sounds.playSound("powerUp3");
				break;
			case paddleSmall:
				resizePaddle(-1, false);
				Sounds.playSound("badPowerUp2");
				break;
			case ballSpeedFast:
				for (GamePart gp : ((Board) p).getGameParts()) {
					if (gp instanceof Ball) {
						((Ball) gp).speedChange = 4;
						((Ball) gp).speedChanged = true;
					}
				}
				Sounds.playSound("powerUp4");
				break;
			case ballSpeedSlow:
				for (GamePart gp : ((Board) p).getGameParts()) {
					if (gp instanceof Ball) {
						((Ball) gp).speedChange = -4;
						((Ball) gp).speedChanged = true;
					}
				}
				Sounds.playSound("badPowerUp1");
				break;
			case paddleOppositeMouse:
				Board.reversedPaddle = true;
				Sounds.playSound("badPowerUp3");
				break;
			case paddleSticky:
				for (GamePart gp : ((Board) p).getGameParts()) {
					if (gp instanceof Ball) {
						((Ball) gp).stickyChanged = true;
					}
				}
				Sounds.playSound("powerUp1");
				break;
			case fireBall:
				Brick.isFireMode = true;
				Sounds.playSound("thunder");
				break;
			case moreBalls:
				addBall(p);
				Sounds.playSound("powerUp2");
				break;
			case shoterPaddle:
				activateGuns(p, 10000);
				Sounds.playSound("gunReload");
				break;
			case wallBehindPaddle:
				activateWall(p, 10000);
				Sounds.playSound("powerUp5");
				break;
			case bomb:
				Board.lifes--;
				((Board) p).addEffect(new Effect(p, x + width, y, "explosion", 300));
				Board.isPaddleSuspended = true;
				Sounds.playSound("bomb");
				break;
			default:
				break;
			}
		}
	}
	
	public static void resizePaddle(int sizeMultiplyer, boolean isCheat)
	{
		Paddle.cheatMode = isCheat;
		Paddle.paddleChanged = true;
		Paddle.paddleSizeChanger = sizeMultiplyer;
	}

	public static void activateGuns(JPanel p, int time) {
		if (time > 0) {
			((Board) p).addLongTermGamePart(new Gun(p, -1, time));
			((Board) p).addLongTermGamePart(new Gun(p, 1, time));
		} else {
			((Board) p).addLongTermGamePart(new Gun(p, -1));
			((Board) p).addLongTermGamePart(new Gun(p, 1));
		}
	}
	
	public static void activateWall(JPanel p, int time)
	{
		if (time > 0) {
			((Board) p).addShortTermGamePart(new Wall(p, time));
		} else {
			((Board) p).addShortTermGamePart(new Wall(p));
		}
	}
	
	public static void addBall(JPanel p)
	{
		((Board) p).addLongTermGamePart(new Ball(p, false));
	}

	private synchronized boolean hasColided() {
		for (GamePart gp : ((Board) p).getGameParts()) {
			if (gp instanceof Paddle) {
				if (gp.getY() <= y + height && gp.getY() + gp.getHeight() >= y && gp.getX() <= x + width
						&& gp.getX() + gp.getWidth() >= x) {
					return true;
				}
			}
		}

		return false;
	}

	public TYPES randomizeType() {
		int num = (int) (Math.random() * 2000 - 500);
		//System.out.println(num + "");

		 //return TYPES.bomb;/*

		// good: extraLife, paddleBig, fireBall, moreBalls, wallBehindPaddle,
		// shoterPaddle
		// bad: paddleSmall, paddleOppositeMouse
		// neutral: paddleSticky, BallSpeedSlow, BallSpeedFast
		if (num < 180)
			return TYPES.NONE;// no powerUp
		else if (num >= 210 && num < 240)
			return TYPES.extraLife;
		else if (num >= 240 && num < 300)
			return TYPES.paddleBig;
		else if (num >= 300 && num < 360)
			return TYPES.paddleSmall;
		else if (num >= 360 && num < 420)
			return TYPES.ballSpeedFast;
		else if (num >= 420 && num < 480)
			return TYPES.ballSpeedSlow;
		else if (num >= 480 && num < 540)
			return TYPES.paddleOppositeMouse;
		else if (num >= 540 && num < 570)
			return TYPES.paddleSticky;
		else if (num >= 570 && num < 600)
			return TYPES.fireBall;
		else if (num >= 600 && num < 630)
			return TYPES.moreBalls;
		else if (num >= 630 && num < 650)
			return TYPES.shoterPaddle;
		else if (num >= 650 && num < 670)
			return TYPES.wallBehindPaddle;
		else if(num >= 670 && num < 700)
			return TYPES.bomb;
		return TYPES.NONE;// */
	}

	public void draw(Graphics g) {
		g.setColor(color);
		width = p.getWidth() / 40;
		height = p.getHeight() / 55;
		
		
		if(icon != null)
		{
			Image img = icon.getImage();
			float realativeDifference = (float)img.getWidth(null) / img.getHeight(null);
			height = Math.round(width / realativeDifference );
			g.drawImage(img, x + width / 2, y, width, height, null);
		}
		else
		{
			g.fillRect(x + width / 2, y, width, height);
		}
		
		g.setColor(Color.white);
		// g.drawString(type < 10? "0" + type : "" + type, x + width / 2 - 4, y
		// + (int)(height / 2) + 5);

	}
	
	public ArrayList<String> getSaveData() {
		ArrayList<String> data = new ArrayList<>();
		data.add("PowerUp{");
		data.add("type=" + type.name());
		data.add("color=" + color.getRGB());
		data.add("icon=" + location);
		ArrayList<String> tmp = super.getSaveData();
		for(String info : tmp)
		{
			data.add(info);
		}
		data.add("}");
		return data;
	}
	
	public void loadDataFromSave(HashMap<String, String> data) {
		super.loadDataFromSave(data);
		type = Enum.valueOf(TYPES.class, data.get("type"));
		color = new Color(Integer.parseInt(data.get("color")));
		if(!data.get("icon").equals(""))
			icon = new ImageIcon(data.get("icon"));
	}

}
