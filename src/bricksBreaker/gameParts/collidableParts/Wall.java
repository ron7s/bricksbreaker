package bricksBreaker.gameParts.collidableParts;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.Sounds;
import bricksBreaker.gameParts.GamePart;
import bricksBreaker.gameParts.collidableParts.PowerUp.TYPES;

public class Wall extends GamePart {
	int timeToLive;
	boolean sholdDie;
	public Wall(JPanel panel) {
		super(panel);
		timeToLive = 1;
		sholdDie = false;
	}
	public Wall(JPanel panel, int timeToLive) {
		super(panel);
		this.timeToLive = timeToLive;
		sholdDie = true;
	}
	
	public void run(){
		while(Board.lifes > 0 && timeToLive > 0 && level == Board.level)
		{
			super.run();
			if(sholdDie)
			{
				timeToLive -= delay;
			}
			checkCollision();
		}
	}
	
	public void checkCollision()
	{
		for (GamePart gp : ((Board) p).getGameParts()) {
			if(gp instanceof Ball)
			{
				Ball b = (Ball) gp;
				if (b.getY() + b.getWidth() >= y) {
					b.setSpeed(b.getDx(), b.getDy() * -1);
					b.setPoint(b.getX(), y - b.getWidth());
					Sounds.playSound("hit");
				}
			}
		}
	}
	
	public void draw(Graphics g) {
		x = 0;
		y = p.getHeight() - 20;
		height = 20;
		width = p.getWidth();
		g.setColor(Color.green);
		g.fillRect(x, y, width, height);
	}
	
	public ArrayList<String> getSaveData() {
		ArrayList<String> data = new ArrayList<>();
		data.add("Wall{");
		data.add("timeToLive=" + timeToLive);
		data.add("sholdDie=" + sholdDie);
		ArrayList<String> tmp = super.getSaveData();
		for(String info : tmp)
		{
			data.add(info);
		}
		data.add("}");
		return data;
	}
	
	public void loadDataFromSave(HashMap<String, String> data) {
		super.loadDataFromSave(data);
		timeToLive = Integer.parseInt(data.get("timeToLive"));
		sholdDie = Boolean.parseBoolean(data.get("sholdDie"));
	}
}
