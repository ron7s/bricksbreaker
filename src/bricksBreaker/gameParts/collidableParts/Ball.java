package bricksBreaker.gameParts.collidableParts;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.Sounds;
import bricksBreaker.gameParts.GamePart;

public class Ball extends GamePart {
	int dx, dy, originalSpeed = 12, speed = 12, speedChange = 0,
			timePassed = 0, timeSpeedChanged = 0, timeStickyChanged = 0;
	public boolean speedChanged = false, stickyChanged = false;
	boolean isActive, activeUntilDeath = false, isOriginal, hasDied = false;
	boolean reqSpeedChangeX = false, reqSpeedChangeY = false;

	public Ball(JPanel panel, boolean isOriginal) {
		super(panel);
		this.setPriority(4);
		isActive = false;
		this.height = this.width;
		this.isOriginal = isOriginal;
		dx = dy = 0;
	}

	public void run() {
		while (Board.lifes > 0 && (isOriginal || !hasDied) && level == Board.level) {
			super.run();
			if (!isActive) {
				dx = dy = 0;
				for (GamePart gp : ((Board) p).getGameParts()) {
					if (gp instanceof Paddle && ((Paddle) gp).getLocation() == 0) {
						x = gp.getX() + gp.getWidth() / 2 - width / 2;
						y = gp.getY() - width;
					}
				}
			} else {
				if(reqSpeedChangeX) {
					reqSpeedChangeX = false;
					dx *= -1;
				}
				if(reqSpeedChangeY) {
					reqSpeedChangeY = false;
					dy *= -1;
				}
				x += dx;
				y += dy;
				if (dy == 0)
					dy = -speed;
				if (Board.difficulty < 2) {
					if (x <= 0) {
						x = 0;
						dx *= -1;
						Sounds.playSound("hit");
					} else if (x + width >= p.getWidth()) {
						x = p.getWidth() - width;
						dx *= -1;
						Sounds.playSound("hit");
					}
				}
				if (Board.difficulty == 0) {
					if (y <= 0) {
						y = 0;
						dy *= -1;
						Sounds.playSound("hit");
					}
				}
				if(y >= p.getHeight()) {
					hasDied = true;
					isActive = false;
					if(isOriginal && level == Board.level) Board.lifes--;
					Sounds.playSound("loseBall");
				}
			}
			timePassed += delay;
			if(speedChanged)
			{
				speedChanged = false;
				speed = originalSpeed + speedChange;
				timeSpeedChanged = timePassed;
			}
			else if((timePassed - timeSpeedChanged) == 5000)
			{
				speed = originalSpeed;
				speedChange = 0;
			}
			if(stickyChanged)
			{
				stickyChanged = false;
				activeUntilDeath = true;
				timeStickyChanged = timePassed;
			}
			else if((timePassed - timeStickyChanged) == 5000)
			{
				activeUntilDeath = false;
			}
		}
	}

	public void activate() {
		isActive = true;
	}
	
	public boolean isSticky(){
		return activeUntilDeath;
	}
	
	public void deActivate(){
		isActive = false;
	}
	
	public void requestXSpeedChange() {
		reqSpeedChangeX = true;
	}
	public void requestYSpeedChange() {
		reqSpeedChangeY = true;
	}

	public void setSpeed(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}
	
	public void setBaseSpeed(int speed) {
		originalSpeed = this.speed = speed;
	}

	public int getBaseSpeed() {
		return speed;
	}

	public boolean isActive() {
		return isActive;
	}

	public int getDx() {
		return dx;
	}

	public int getDy() {
		return dy;
	}

	public void draw(Graphics g) {
		width = p.getHeight() / 40;
		g.setColor(Color.black);
		if(isOriginal) g.setColor(Color.red);
		g.fillOval(x, y, width, width);
	}
	
	public ArrayList<String> getSaveData() {
		ArrayList<String> data = new ArrayList<>();
		data.add("Ball{");
		data.add("dx=" + dx);
		data.add("dy=" + dy);
		data.add("speedChange=" + speedChange);
		data.add("timePassed=" + timePassed);
		data.add("timeSpeedChanged=" + timeSpeedChanged);
		data.add("timeStickyChanged=" + timeStickyChanged);
		
		data.add("speedChanged=" + speedChanged);
		data.add("stickyChanged=" + stickyChanged);
		data.add("isActive=" + isActive);
		data.add("activeUntilDeath=" + activeUntilDeath);
		data.add("isOriginal=" + isOriginal);
		data.add("hasDied=" + hasDied);
		data.add("reqSpeedChangeX=" + reqSpeedChangeX);
		data.add("reqSpeedChangeY=" + reqSpeedChangeY);
		
		ArrayList<String> tmp = super.getSaveData();
		for(String info : tmp)
		{
			data.add(info);
		}
		data.add("}");
		return data;
	}
	
	public void loadDataFromSave(HashMap<String, String> data) {
		super.loadDataFromSave(data);
		dx = Integer.parseInt(data.get("dx"));
		dy = Integer.parseInt(data.get("dy"));
		speedChange = Integer.parseInt(data.get("speedChange"));
		timePassed = Integer.parseInt(data.get("timePassed"));
		timeSpeedChanged = Integer.parseInt(data.get("timeSpeedChanged"));
		timeStickyChanged = Integer.parseInt(data.get("timeStickyChanged"));
		
		speedChanged = Boolean.parseBoolean(data.get("speedChanged"));
		stickyChanged = Boolean.parseBoolean(data.get("stickyChanged"));
		isActive = Boolean.parseBoolean(data.get("isActive"));
		activeUntilDeath = Boolean.parseBoolean(data.get("activeUntilDeath"));
		isOriginal = Boolean.parseBoolean(data.get("isOriginal"));
		hasDied = Boolean.parseBoolean(data.get("hasDied"));
		reqSpeedChangeX = Boolean.parseBoolean(data.get("reqSpeedChangeX"));
		reqSpeedChangeY = Boolean.parseBoolean(data.get("reqSpeedChangeY"));
	}

}
