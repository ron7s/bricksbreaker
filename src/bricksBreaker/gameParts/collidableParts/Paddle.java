package bricksBreaker.gameParts.collidableParts;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.Sounds;
import bricksBreaker.gameParts.GamePart;

public class Paddle extends CollidablePart {
	private int location, timePassed = 0, timeChanged = 0;
	public static int paddleSizeChanger = 0;
	public static boolean paddleChanged = false, cheatMode = false;

	public Paddle(JPanel panel, int location) {
		super(panel);
		this.location = location;
	}

	@SuppressWarnings("unchecked")
	public void run() {
		while (Board.lifes > 0 && level == Board.level) {
			super.run();
			ArrayList<GamePart> tmp;
			synchronized (((Board) p).getGameParts()) {
				tmp = (ArrayList<GamePart>) ((Board) p).getGameParts().clone();
			}
			for (GamePart gp : tmp) {
				if (gp instanceof Ball) {
					int speed = ((Ball) gp).speed;
					// -speed__-speed/2 -speed/2__-speed speed/2__-speed speed__-speed/2
					int part = width / 4;
					/// paddle parts
					if((location % 2 == 0) ? (gp.getY() + gp.getWidth() > y && gp.getY() <= y + height) 
							: (gp.getY() <= y + height) && gp.getY() >= y){
						// part 1
						if (gp.getX() + gp.getWidth() >= x && gp.getX() <= x + part) {
							((Ball) gp).setSpeed((int)(-speed * 0.75), (int)(-speed * 0.25));
							gp.setPoint(gp.getX(), y - gp.getWidth());
							Sounds.playSound("hit");
						}
						// part 2
						if (gp.getX() >= x + part && gp.getX() <= x + part * 2) {
							((Ball) gp).setSpeed((int)(-speed * 0.5), (int)(-speed * 0.5));
							gp.setPoint(gp.getX(), y - gp.getWidth());
							Sounds.playSound("hit");
						}
						// part 3
						if (gp.getX() >= x + part * 2 && gp.getX() <= x + part * 3){
							((Ball) gp).setSpeed((int)(speed * 0.5), (int)(-speed * 0.5));
							gp.setPoint(gp.getX(), y - gp.getWidth());
							Sounds.playSound("hit");
						}
						// part 4
						if (gp.getX() >= x + part * 3 && gp.getX() <= x + width) {
							((Ball) gp).setSpeed((int)(speed * 0.75), (int)(-speed * 0.25));
							gp.setPoint(gp.getX(), y - gp.getWidth());
							Sounds.playSound("hit");
						}
						if(((Ball) gp).isSticky()) ((Ball) gp).deActivate();
					}
					
				}
			}
			timePassed += delay;
			if(paddleChanged)
			{
				paddleChanged = false;
				timeChanged = timePassed;
			}
			else if(!cheatMode && (timePassed - timeChanged) >= 5000)
			{
				timePassed = 0;
				paddleSizeChanger = 0;
			}
		}
	}

	public void draw(Graphics g) {
		g.setColor(Color.black);
		int width = p.getWidth() / (10 - (paddleSizeChanger * 4));
		int height = p.getHeight() / 35;
		if (location < 2) {
			y = ((p.getHeight() - (height * 2)) * (1 - location) + location * height);
			if (x > p.getWidth() - width)
				x = p.getWidth() - width;
			else if (x < 0)
				x = 0;
		} else {
			height += width;
			width = height - width;
			height -= width;
			x = ((p.getWidth() - (width * 2)) * (1 - (location - 2)) + (location - 2) * width);
			if (y > p.getHeight() - height)
				y = p.getHeight() - height;
			else if (y < 0)
				y = 0;
		}
		this.width = width;
		this.height = height;
		g.fillRect(x, y, width, height);
	}

	public int getLocation() {
		return location;
	}
	
	public ArrayList<String> getSaveData() {
		ArrayList<String> data = new ArrayList<>();
		data.add("Paddle{");
		data.add("location=" + location);
		data.add("timePassed=" + timePassed);
		data.add("timeChanged=" + timeChanged);
		data.add("paddleSizeChanger=" + paddleSizeChanger);
		data.add("paddleChanged=" + paddleChanged);
		data.add("cheatMode=" + cheatMode);
		ArrayList<String> tmp = super.getSaveData();
		for(String info : tmp)
		{
			data.add(info);
		}
		data.add("}");
		return data;
	}
	
	public void loadDataFromSave(HashMap<String, String> data) {
		super.loadDataFromSave(data);
		location = Integer.parseInt(data.get("location"));
		timePassed = Integer.parseInt(data.get("timePassed"));
		timeChanged = Integer.parseInt(data.get("timeChanged"));
		paddleSizeChanger = Integer.parseInt(data.get("paddleSizeChanger"));
		paddleChanged = Boolean.parseBoolean(data.get("paddleChanged"));
		cheatMode = Boolean.parseBoolean(data.get("cheatMode"));
	}

}
