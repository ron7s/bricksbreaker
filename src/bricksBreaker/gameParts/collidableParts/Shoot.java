package bricksBreaker.gameParts.collidableParts;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.Sounds;
import bricksBreaker.gameParts.GamePart;
import bricksBreaker.gameParts.collidableParts.PowerUp.TYPES;

public class Shoot extends GamePart {
	boolean sholdDie;
	int speed = 10;
	public Shoot(JPanel panel, int x, int y) {
		super(panel);
		this.x = x;
		this.y = y;
		sholdDie = false;
	}
	
	public void run(){
		Sounds.playSound("gunshot");
		while(Board.lifes > 0 && y > 0 && !sholdDie && level == Board.level)
		{
			super.run();
			y -= speed;
		}
	}
	
	public void kill()
	{
		sholdDie = true;
	}
	
	public void draw(Graphics g) {
		height = 10;
		width = 5;
		g.setColor(Color.orange);
		g.fillRect(x, y, width, height);
	}
	
	public ArrayList<String> getSaveData() {
		ArrayList<String> data = new ArrayList<>();
		data.add("Shoot{");
		data.add("sholdDie=" + sholdDie);
		ArrayList<String> tmp = super.getSaveData();
		for(String info : tmp)
		{
			data.add(info);
		}
		data.add("}");
		return data;
	}
	
	public void loadDataFromSave(HashMap<String, String> data) {
		super.loadDataFromSave(data);
		sholdDie = Boolean.parseBoolean(data.get("sholdDie"));
	}

}
