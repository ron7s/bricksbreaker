package bricksBreaker.gameParts.collidableParts;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import bricksBreaker.Board;

public class CreatorBrick extends Brick {
	boolean isVisible = false;
	public CreatorBrick(JPanel panel, int streanth, int x, int y, Image img) {
		super(panel, streanth, x, y, img);
	}
	
	public void run() {
		while(Board.isEditMode)
		{
			try {
				sleep(delay);
			} catch (Exception e) {}
			checkClick();
		}
	}
	
	public void draw(Graphics g) {
		if(isVisible)
		{
			super.draw(g);
		}
		else
		{
			width = p.getWidth() / 20;
			height = p.getHeight() / 40;
			ax = x * (width);
			ay = y * (height) + width;
			g.setColor(Color.black);
			g.drawRect(ax, ay, width, height);
		}
	}
	
	public void checkClick(){
		int wi = width;
		if (Board.getMouseY() <= ay + height && Board.getMouseY() >= ay && Board.getMouseX() >= ax
				&& Board.getMouseX() <= ax + wi && Board.isLeftMouseButtonPressed) {
			isVisible = true;
			try {
				streanth = Integer.parseInt(((Board)p).getExtraInfo().get("creatorBrickStrength"));
			}
			catch (Exception e) {
				streanth = 1;
			}
			if(streanth > 99) streanth = 99;
			if(streanth < 1)
			{
				streanth = 0;
				isVisible = false;
			}
		}
	}

}
