package bricksBreaker.gameParts.collidableParts;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.Sounds;
import bricksBreaker.gameParts.GamePart;

public class Brick extends GamePart {
	int streanth;
	Color color;
	Image img;
	int ax, ay;
	public static boolean isFireMode = false;
	boolean wasAlive = false;

	public Brick(JPanel panel, int streanth, int x, int y, Image img) {
		super(panel);
		this.setPriority(9);
		this.x = x;
		this.y = y;
		this.streanth = streanth;
		this.img = img;
		color = Board.RandomizeColor();
	}

	public void run() {
		while (streanth != 0 && Board.lifes > 0 && level == Board.level) {
			wasAlive = true;
			super.run();
			if (hasColided() && Board.ballHitDelay <= 0) {
				//Board.ballHitDelay = 50;
				streanth--;
				if(isFireMode) streanth = 0;
				if(streanth > 0) Sounds.playSound("brickHit");
			}
		}
		if(wasAlive)
		{
			((Board)p).addShortTermGamePart(new PowerUp(p, ax, ay, level));
			Sounds.playSound("brickBroken");
		}
		
	}

	public void draw(Graphics g) {
		g.setColor(color);
		width = p.getWidth() / Board.bricksInBoard;
		height = p.getHeight() / (Board.bricksInBoard * 2);
		ax = x * (width);
		ay = y * (height) + width;
		g.fillRect(ax, ay, width, height);
		g.setColor(Color.white);
		// g.drawRect(ax, ay, width - space, height);
		g.drawImage(img, ax, ay, width, height, null);
		Font font = new Font("Ariel", Font.BOLD, 12);
		g.setFont(font);
		g.drawString((streanth > 9) ? "" + streanth : "0" + streanth, ax + (int)Math.round(width / 2.5), ay + height / 2 + 6);
		g.setColor(Color.black);
		g.drawRect(ax, ay, width, height);
	}

	@SuppressWarnings("unchecked")
	private boolean hasColided() {
		int wi = width;
		for (GamePart gp : ((Board) p).getGameParts()) {
			if (gp instanceof Ball && gp.isAlive() && ((Ball) gp).isActive()) {
				Ball b = (Ball)gp;
				int nextBallY = b.getY() + b.getDy();
				int nextBallX = b.getX() + b.getDx();
				if(nextBallY <= ay + height && nextBallY + b.getWidth() >= ay &&
						nextBallX + b.getWidth() >= ax && nextBallX <= ax + wi) {
					if(!isFireMode) {
						if(b.getY() > ay + height || b.getY() + b. getWidth() < ay) {
							b.requestYSpeedChange();
						}
						if(b.getX() + b.getWidth() < ax || b.getX() > ax + wi) {
							b.requestXSpeedChange();
						}
					}
					return true;
				}
			}
		}
		ArrayList<GamePart> tmp;
		synchronized(((Board) p).getShortTermGameParts())
		{
			tmp = (ArrayList<GamePart>) ((Board) p).getShortTermGameParts().clone();
		}
		for (GamePart gp : tmp) {
			if(gp != null && gp.isAlive() && gp instanceof Shoot)
			{
				if (gp.getY() <= ay + height && gp.getY() + gp.getHeight() >= ay && gp.getX() + gp.getWidth() >= ax
						&& gp.getX() <= ax + wi) {
					gp.kill();
					return true;
				}
			}
		}
		
		return false;
	}
	
	public int getBrickStreanth(){
		return streanth;
	}

}
