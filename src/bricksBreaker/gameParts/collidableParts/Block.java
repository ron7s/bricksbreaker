package bricksBreaker.gameParts.collidableParts;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.ArrayList;

import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.gameParts.GamePart;

public class Block extends CollidablePart {

	public static enum SHAPES {
		RECTANGLE, ELLIPSE, TRIANGLE, CUSTOM
	}

	ArrayList<Integer> xPoints, yPoints;
	SHAPES shape;
	
	int angle;
	int brickWidth;
	int brickHeight;

	public Block(JPanel panel, SHAPES shape) {
		super(panel);
		this.shape = shape;
		this.angle = 0;
	}

	public void run() {
		while (Board.lifes > 0 && level == Board.level) {
			super.run();
			
			checkCollision();
		}
	}

	private void clearPoints() {
		xPoints = new ArrayList<>();
		yPoints = new ArrayList<>();
	}

	public void setPoints() {
		double radAngle = (angle / 180.0 * Math.PI);
		brickWidth = p.getWidth() / Board.bricksInBoard;
		brickHeight = p.getHeight() / (Board.bricksInBoard * 2);
		
		int aWidth = width * brickWidth;
		int aHeight = height * brickHeight;
		int ax = (x * brickWidth + (brickWidth / 2)) - (int) (Math.cos(radAngle) * aWidth / 2) + (int) (Math.sin(radAngle) * aHeight / 2);
		int ay = (y * brickHeight + brickWidth) - (int) (Math.cos(radAngle) * aHeight / 2) - (int) (Math.sin(radAngle) * aWidth / 2);
		switch (shape) {
		case RECTANGLE:
			clearPoints();
			addPoint(ax, ay);
			ax += (int) (Math.cos(radAngle) * aWidth);
			ay += (int) (Math.sin(radAngle) * aWidth);
			addPoint(ax, ay);
			ax -= (int) (Math.sin(radAngle) * aHeight);
			ay += (int) (Math.cos(radAngle) * aHeight);
			addPoint(ax, ay);
			ax -= (int) (Math.cos(radAngle) * aWidth);
			ay -= (int) (Math.sin(radAngle) * aWidth);
			addPoint(ax, ay);
			break;
		case TRIANGLE:
			clearPoints();
			addPoint(ax, ay);
			ax += (int) (Math.cos(radAngle) * aWidth);
			ay += (int) (Math.sin(radAngle) * aWidth);
			addPoint(ax, ay);
			ax -= (int) (Math.cos(radAngle) * aWidth / 2) + (int) (Math.sin(radAngle) * aHeight);
			ay += (int) (Math.cos(radAngle) * aHeight) - (int) (Math.sin(radAngle) * aWidth / 2);
			addPoint(ax, ay);
			break;
		default:
			break;
		}
		clearSides();
		for(int i = 0; i < xPoints.size(); i++)
		{
			int nxt = i + 1;
			if(nxt == xPoints.size()) nxt = 0;
			addSide(xPoints.get(i), yPoints.get(i), xPoints.get(nxt), yPoints.get(nxt), 0, false);
		}
		
	}

	public void addPoint(int x, int y) {
		xPoints.add(x);
		yPoints.add(y);
	}

	public void draw(Graphics g) {
		g.setColor(Color.black);
		int aWidth = width * brickWidth;
		int aHeight = height * brickHeight;
		if(shape == SHAPES.ELLIPSE) {
			g.fillArc(x - aWidth / 2, y - aHeight / 2, aWidth, aHeight, 0, 360);
		}
		else
		{
			int size = xPoints.size();
			int[] xpoints = new int[size];
			int[] ypoints = new int[size];
			int customAxiesChanger = (shape == SHAPES.CUSTOM)? 1:0;
			for(int i = 0; i < size; i++) {
				xpoints[i] = xPoints.get(i) + customAxiesChanger * (x * aWidth);
				ypoints[i] = yPoints.get(i) + customAxiesChanger * (y * aHeight);
			}
			g.fillPolygon(new Polygon(xpoints, ypoints, size));
		}
	}
	
	public synchronized void setPoint(int x, int y) {
		super.setPoint(x, y);
		setPoints();
	}

	public void setSize(int width, int height) {
		super.setSize(width, height);
		setPoints();
	}
	/**
	 * set the rotation of the shape.
	 * effects triangle and rectangle only.
	 * @param angle the angle to rotate the shape to in degrees.
	 */
	public void setAngle(int angle) {
		this.angle = angle;
		setPoints();
	}
	public int getAngle() {
		return angle;
	}
	public SHAPES getShape() {
		return shape;
	}

	@Override
	public String toString() {
		
		String str = shape.name() + ":" + x + ":" + y + ":" + width + ":" + height + ":" + angle;
		if(shape == SHAPES.CUSTOM)
		{
			str = shape.name() + ":" + x + ":" + y;
			int size = xPoints.size();
			for(int i = 0; i < size; i++)
			{
				str += ":" + xPoints.get(i) + ":" + yPoints.get(i);
			}
		}
		return str;
	}
	
}
