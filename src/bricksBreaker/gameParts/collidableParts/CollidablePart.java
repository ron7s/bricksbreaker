package bricksBreaker.gameParts.collidableParts;

import java.awt.Point;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.ExtraMath;
import bricksBreaker.Sounds;
import bricksBreaker.Vector;
import bricksBreaker.gameParts.GamePart;

public class CollidablePart extends GamePart {

	class Point2 {
		int x, y;

		public Point2(int x, int y) {

		}
	}

	class colidableSide {
		int x1, y1, x2, y2, angle;
		boolean useAngle;

		public colidableSide(int x1, int y1, int x2, int y2, int angle, boolean useAngle) {
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			this.angle = angle;
			this.useAngle = useAngle;
		}
	}

	ArrayList<colidableSide> sides = new ArrayList<>();

	public CollidablePart(JPanel panel) {
		super(panel);
	}

	public void addSide(int x1, int y1, int x2, int y2, int angle, boolean useAngle) {
		sides.add(new colidableSide(x1, y1, x2, y2, angle, useAngle));
		//System.out.println(x1 + "," + y1 + ",,," + x2 + "," + y2 + ",,," + angle);
	}

	public void addSide(Point p1, Point p2, int angle, boolean useAngle) {
		sides.add(
				new colidableSide((int) p1.getX(), (int) p1.getY(), (int) p2.getX(), (int) p2.getY(), angle, useAngle));
	}
	
	public void clearSides() {
		sides.clear();
	}

	public boolean checkCollision() {
		for (GamePart gp : ((Board) p).getGameParts()) {
			if (gp instanceof Ball && ((Ball) gp).isActive()) {
				Ball b = (Ball) gp;
				int ballX = (b.getX() + b.getDx()) + (b.getWidth() / 2);
				int ballY = (b.getY() + b.getDy()) + (b.getHeight() / 2);

				for (colidableSide side : sides) {
					double distBallSides = ExtraMath.sumDistanceFromTwoPoints(side.x1, side.y1, side.x2, side.y2, ballX, ballY);
					double distLine = ExtraMath.getDistance(side.x1, side.y1, side.x2, side.y2);
					Vector ball = new Vector(b.getDx(), b.getDy());
					Vector line = new Vector(side.x1 - side.x2, side.y1 - side.y2);
					if(distBallSides < distLine + b.getWidth() * 0.6 && distBallSides > distLine - b.getWidth() * 0.6)
					{
						double angle = Math.asin((side.y1 - side.y2) / distLine);
						if(side.useAngle) angle = side.angle / 180 * Math.PI;
						
						
						Point p = ExtraMath.getNewSpeeds(b.getDx(), b.getDy(), angle);
						//Point p = ExtraMath.getNewSpeeds(ball, line.normal());
						b.setSpeed((int)p.getX(), (int)p.getY());
						Sounds.playSound("hit");
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public ArrayList<String> getSaveData() {
		return super.getSaveData();
	}
	
	public void loadDataFromSave(HashMap<String, String> data) {
		super.loadDataFromSave(data);
	}

}
