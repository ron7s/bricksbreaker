package bricksBreaker.gameParts;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import bricksBreaker.Board;

public class Effect extends GamePart {
	int timePassed = 0;
	int timeToLife;
	Image img;
	public Effect(JPanel panel, int x, int y, String effectName, int timeInMillis) {
		super(panel);
		timeToLife = timeInMillis;
		this.x = x;
		this.y = y;
		img = new ImageIcon("imgs/" + effectName + (effectName.contains(".") ? "" : ".gif")).getImage();
	}
	
	public void run(){
		while(timePassed < timeToLife)
		{
			super.run();
			timePassed += delay;
		}
		((Board) p).removeEffect(this);
	}
	
	public void draw(Graphics g) {
		width = 100;//img.getWidth(null);
		float realativeDifference = (float)img.getWidth(null) / img.getHeight(null);
		height = Math.round(width / realativeDifference );
		g.drawImage(img, x - width / 2, y - height / 2, width, height, null);
	}
}
