package bricksBreaker.gameParts.killableParts;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.font.GlyphVector;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import bricksBreaker.Board;
import bricksBreaker.gameParts.GamePart;

public class GameTextBox extends GamePart implements KillablePart {
	ArrayList<String> specielKeys = new ArrayList<>();
	String txt, visibleTxt;
	int txtStart, caretPos, caretSubstracter, markerStart, markerEnd;
	boolean sholdDie, isWritable, isSelected, caretVisibile, mouseHover;
	static int blinkTime = 500;
	int timePassed;
	Graphics g;
	CaretListener func;

	public GameTextBox(JPanel panel) {
		super(panel);
		initVars(true);
	}

	public GameTextBox(JPanel panel, CaretListener func) {
		super(panel);
		initVars(true);
		setText(txt);
		this.func = func;
	}

	public GameTextBox(JPanel panel, String txt, boolean isWritable) {
		super(panel);
		initVars(isWritable);
		setText(txt);
	}

	private void initVars(boolean isWritable) {
		mouseHover = false;
		this.isWritable = isWritable;
		isSelected = caretVisibile = false;
		height = 20;
		timePassed = 0;
		txt = visibleTxt = "";
		txtStart = caretPos = caretSubstracter = 0;
		markerStart = markerEnd = 0;
	}

	public void run() {
		while (!sholdDie) {
			super.run();
			timePassed += delay;
			checkMouseHover();
			checkClick();
			if (timePassed >= blinkTime) {
				timePassed = 0;
				caretVisibile = !caretVisibile;
			}

			Rectangle2D bounds = getTextBounds(txt, txtStart, caretPos);
			int strWidth = (int) bounds.getWidth();
			if (!isWritable)
				width = strWidth + 7;
			while (strWidth > width - 6) {
				txtStart++;
				bounds = getTextBounds(txt, txtStart, caretPos - caretSubstracter);
				strWidth = (int) bounds.getWidth();

			}
			try {
				visibleTxt = txt.substring(txtStart, caretPos);
			}
			catch (Exception e) {
				if(caretPos > txt.length())
					caretPos = txt.length();
				if(txtStart > caretPos)
					txtStart--;
				if(txtStart < 0)
					txtStart = 0;
				if(caretPos - caretSubstracter < txtStart)
					caretSubstracter--;
			}
		}
	}

	public void draw(Graphics g) {
		Color transperent = new Color(0, 0, 0, 0);
		this.g = g;
		if (isWritable)
			g.setColor(Color.white);
		else
			g.setColor(transperent);
		g.fillRect(x, y, width, height);
		if (isWritable)
			g.setColor(Color.gray);
		else
			g.setColor(transperent);
		g.drawRect(x, y, width, height);
		if (markerStart != markerEnd && isWritable) {
			int startPos = (txtStart < markerStart) ? markerStart : txtStart;
			int endPos = (caretPos > markerEnd) ? markerEnd : caretPos;
			Rectangle2D bounds = getTextBounds(txt, startPos, endPos);
			Rectangle2D startBound = getTextBounds(txt, txtStart, startPos);
			int space = (int) startBound.getWidth();
			int strWidth = (int) bounds.getWidth() + 2;
			g.setColor(new Color(0, 120, 215, 150));
			g.fillRect(x + 3 + space, y + 2, strWidth, height - 3);
		}
		g.setColor(Color.black);
		if (isSelected && caretVisibile && isWritable) {
			Rectangle2D bounds = getTextBounds(txt, txtStart, caretPos - caretSubstracter);
			int strWidth = (int) bounds.getWidth();
			g.fillRect(x + strWidth + 4, y + 2, 2, height - 3);
		}
		g.drawString(visibleTxt, x + 3, y + 15);
	}

	public Rectangle2D getTextBounds(String txt, int startPos, int endPos) {
		Graphics2D g2d = (Graphics2D) g;
		if (g == null)
			return new Rectangle(0, 0, 0, 0);
		Font f = g.getFont();
		String tmp;
		try {
			tmp = txt.substring(startPos, endPos);
		} catch (Exception e) {
			tmp = "";
		}
		GlyphVector gv = f.layoutGlyphVector(g2d.getFontRenderContext(), tmp.toCharArray(), 0, tmp.length(),
				GlyphVector.FLAG_MASK);
		return gv.getVisualBounds();
	}

	@SuppressWarnings("serial")
	public void recieveAction(String key) {
		if (isSelected && isWritable) {
			switch (key) {
			case "Right":
				if (caretSubstracter > 0)
					caretSubstracter--;
				else if (caretPos < txt.length()) {
					txtStart++;
					caretPos++;
				}
				break;
			case "Left":
				if (caretPos - caretSubstracter > txtStart)
					caretSubstracter++;
				else if (txtStart > 0) {
					txtStart--;
					caretPos--;
				}
				break;
			case "Shift":
			case "Ctrl":
				if (!specielKeys.contains(key))
					specielKeys.add(key);
				break;
			case "Backspace":
				if(markerStart != markerEnd)
				{
					removeFromString();
				}
				else if (caretPos - caretSubstracter > 0) {
					txt = txt.substring(0, caretPos - caretSubstracter - 1)
							+ ((caretPos - caretSubstracter < txt.length()) ? txt.substring(caretPos - caretSubstracter)
									: "");
					if (txtStart > 0)
						txtStart--;
					if (caretPos > txt.length())
						caretPos--;
				}
				break;
			case "Delete":
				if(markerStart != markerEnd)
				{
					removeFromString();
				}
				else if (caretPos - caretSubstracter < txt.length()) {
					txt = ((caretPos - caretSubstracter > 0) ? txt.substring(0, caretPos - caretSubstracter) : "")
							+ txt.substring(caretPos - caretSubstracter + 1);
					if (caretPos < txt.length())
						caretPos++;
					if (caretPos > txt.length()) {
						caretPos--;
						caretSubstracter--;
					}

				}
				break;
			default:
				if (key.contains("NumPad-")) {
					key = key.substring("NumPad-".length());
				}
				if (key.length() == 1) {
					if (specielKeys.contains("Ctrl") && isWritable) {
						String marked = "";
						StringSelection selection;
						Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
						switch (key.toLowerCase()) {
						case "a":
							markerStart = 0;
							markerEnd = txt.length();
							break;
						case "c":
							marked = txt.substring(markerStart, markerEnd);
							selection = new StringSelection(marked);
							clipboard.setContents(selection, selection);
							break;
						case "x":
							marked = txt.substring(markerStart, markerEnd);
							selection = new StringSelection(marked);
							clipboard.setContents(selection, selection);
							removeFromString();
							break;
						case "v":
							try {
								marked = (String) clipboard.getContents(this)
										.getTransferData(DataFlavor.getTextPlainUnicodeFlavor());
							} catch (Exception e) {
								marked = "";
							}

							break;
						}
					} else if (specielKeys.contains("Shift")) {
						txt = ((caretPos - caretSubstracter > txtStart) ? txt.substring(0, caretPos - caretSubstracter)
								: "") + key.toUpperCase()
								+ ((caretSubstracter > 0) ? txt.substring(caretPos - caretSubstracter) : "");
					} else {
						txt = ((caretPos - caretSubstracter > txtStart) ? txt.substring(0, caretPos - caretSubstracter)
								: "") + key.toLowerCase()
								+ ((caretSubstracter > 0) ? txt.substring(caretPos - caretSubstracter) : "");
					}
					caretPos++;
				} else
					System.out.println(key);

				if (func != null)
					func.caretUpdate(new CaretEvent(this) {

						@Override
						public int getMark() {
							return markerEnd;
						}

						@Override
						public int getDot() {
							return caretPos - caretSubstracter;
						}
					});
				break;
			}

		}
	}

	public void setPressedLocation(int x) {
		if(!isWritable || !isSelected) return;
		int closestToX = getClosestToX(x);
		markerStart = markerEnd = closestToX;
		while (caretPos - caretSubstracter != closestToX) {
			if (caretPos - caretSubstracter > closestToX)
				caretSubstracter++;
			else
				caretSubstracter--;
		}
	}

	public int getClosestToX(int x) {
		int curX = txtStart;
		Rectangle2D bounds;
		int strWidth = 0;
		while(this.x + strWidth + 4 < x && curX < caretPos) {
			curX++;
			bounds = getTextBounds(txt, txtStart, curX);
			strWidth = (int) bounds.getWidth();
			
		}
		return curX;
	}

	public void removeFromString() {
		txt = txt.substring(0, markerStart) + txt.substring(markerEnd);
		// caretSubstracter = caretPos - markerEnd;
		caretPos -= (markerEnd - markerStart);
		while(caretPos - caretSubstracter < txtStart) {
			caretSubstracter--;
		}
		markerEnd = markerStart;
	}

	public void recieveReleseAction(String key) {
		if (specielKeys.contains(key))
			specielKeys.remove(key);
	}

	public void kill() {
		sholdDie = true;
	}

	public void addCaretListener(CaretListener func) {
		this.func = func;
	}

	public void checkClick() {
		if (Board.isLeftMouseButtonPressed) {
			if (mouseHover)
				isSelected = true;
			else
				isSelected = false;
		}
	}

	public void checkMouseHover() {
		if (Board.getMouseY() <= y + height && Board.getMouseY() >= y && Board.getMouseX() >= x
				&& Board.getMouseX() <= x + width) {
			mouseHover = true;
		} else {
			mouseHover = false;
		}
	}

	public void setText(String txt) {
		this.txt = txt;
		txtStart = 0;
		caretPos = txt.length();
	}

	public String getText() {
		return txt;
	}

	public boolean isMouseHovering() {
		return mouseHover;
	}

}
