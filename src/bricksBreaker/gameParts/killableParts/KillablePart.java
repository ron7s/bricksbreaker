package bricksBreaker.gameParts.killableParts;

public interface KillablePart {
	public void kill();
}
