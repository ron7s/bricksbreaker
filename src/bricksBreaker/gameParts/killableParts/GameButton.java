package bricksBreaker.gameParts.killableParts;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.GlyphVector;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.gameParts.GamePart;

public class GameButton extends GamePart implements KillablePart{
	ActionListener func;
	String txt;
	boolean sholdDie, isWidthSet, isHeightSet;
	public GameButton(JPanel panel, String txt, ActionListener func) {
		super(panel);
		sholdDie = isWidthSet = isHeightSet = false;
		this.txt = txt;
		this.func = func;
	}
	
	public void run(){
		while(!sholdDie){
			try {
				sleep(delay);
			} catch (Exception e) {
				
			}
			checkClick();
		}
	}
	
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
        Font f = g.getFont();
        GlyphVector gv = f.layoutGlyphVector(g2d.getFontRenderContext(), txt.toCharArray(), 0, txt.length(), GlyphVector.FLAG_MASK);
        Rectangle2D bounds = gv.getVisualBounds();
        g2d.translate(x, y);
        int dx = (int)bounds.getX() - 4;
        int dy = (int)bounds.getY() - 4;
        if(!isWidthSet)
        	width = (int)bounds.getWidth();
        if(!isHeightSet)
        	height = (int)bounds.getHeight();
        g.setColor(new Color(0, 0, 255, 60));
        g2d.fillRect(dx, dy, width + 16, height + 16);
        g.setColor(new Color(0, 0, 255, 255));
        g2d.fillRect(dx + 4, dy + 4, width + 8, height + 8);
        g.setColor(Color.white);
        g2d.drawString(txt, dx + 6 + width / 2 - (int)bounds.getWidth() / 2, dy + 16);
        g2d.translate(-x, -y);
	}
	
	public void kill(){
		sholdDie = true;
	}
	
	public void checkClick(){
		if (Board.getMouseY() <= y + height + 4 && Board.getMouseY() >= y - 12 && Board.getMouseX() >= x - 4
				&& Board.getMouseX() <= x + width + 12 && Board.isLeftMouseButtonPressed) {
			while(Board.isLeftMouseButtonPressed){
				try {
					sleep(delay / 3);
				} catch (Exception e) {
					
				}
			}
			cominceClickAction();
		}
	}
	
	public void cominceClickAction(){
		try {
			func.actionPerformed(new ActionEvent(this, 1, "click"));
		} catch (Exception e) {
			
		}
	}
	
	public void setWidth(int width) {
		super.setWidth(width);
		isWidthSet = true;
	}
	public void setHeight(int height) {
		super.setHeight(height);
		isHeightSet = true;
	}
	
	public String getText(){
		return txt;
	}
}
