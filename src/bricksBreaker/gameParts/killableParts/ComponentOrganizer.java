package bricksBreaker.gameParts.killableParts;

import java.util.ArrayList;

import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.gameParts.GamePart;
import bricksBreaker.gameParts.invisibleParts.InvisiblePart;

public class ComponentOrganizer extends InvisiblePart implements KillablePart{
	public static enum Alingment{
		NORMAL, CENTER, OPOSSITE
	}
	public static enum Orientation{
		HORIZONTAL, VERTICAL
	}
	boolean sholdDie = false;
	Alingment xALign, yALign;
	Orientation orientation;
	int marginX, marginY, marginParts;
	ArrayList<GamePart> parts;
	public ComponentOrganizer(JPanel panel, GamePart part, int marginX, int marginY, Alingment xALign, Alingment yAlign) {
		super(panel);
		parts = new ArrayList<>();
		parts.add(part);
		orientation = Orientation.HORIZONTAL;
		initVars(marginX, marginY, xALign, yAlign, 0);
		((Board)p).addLongTermGamePart(part);
	}
	public ComponentOrganizer(JPanel panel, ArrayList<GamePart> parts, Orientation orientation, int marginParts
			, int marginX, int marginY, Alingment xALign, Alingment yAlign, boolean addToGameParts) {
		super(panel);
		this.parts = parts;
		this.orientation = orientation;
		initVars(marginX, marginY, xALign, yAlign, marginParts);
		if(addToGameParts) {
			for(GamePart part : parts) {
				((Board)p).addLongTermGamePart(part);
			}
		}
	}
	
	private void initVars(int marginX, int marginY, Alingment xALign, Alingment yAlign, int marginParts) {
		this.xALign = xALign;
		this.yALign = yAlign;
		this.marginX = marginX;
		this.marginY = marginY;
		this.marginParts = marginParts;
	}
	
	public void run(){
		while(!sholdDie){
			try {
				sleep(delay);
			} catch (Exception e) {
				
			}
			setPoses();
		}
	}
	
	public void setPoses() {
		int sizeAdder = 0;
		int generalSize = 0;
		int mode = (orientation == Orientation.HORIZONTAL)? 1 : 0;
		for(GamePart part : parts) {
			generalSize += part.getWidth() * mode + part.getHeight() * (1 - mode) + marginParts;
		}
		generalSize -= marginParts;
		for(GamePart part : parts) {
			switch(xALign) {
			case OPOSSITE:
				x = p.getWidth() - part.getWidth() - marginX - (sizeAdder * mode);
				break;
			case CENTER:
				x = p.getWidth() / 2 - part.getWidth() * (1 - mode) / 2 + marginX - (generalSize * mode) / 2 + sizeAdder * mode;
				break;
			case NORMAL:
			default:
				x = marginX + (sizeAdder * mode);
				break;
			}
			switch(yALign) {
			case OPOSSITE:
				y = p.getHeight() - part.getHeight() - marginY - (sizeAdder * (1 - mode));
				break;
			case CENTER:
				y = p.getHeight() / 2 - part.getHeight() * mode / 2 + marginY - (generalSize * (1 - mode)) / 2
					+ sizeAdder * (1 - mode);
				break;
			case NORMAL:
			default:
				y = marginY + (sizeAdder * (1 - mode));
				break;
			}
			sizeAdder += (part.getWidth() * mode + part.getHeight() * (1 - mode) + marginParts);
			part.setPoint(x, y);
		}
	}
}
