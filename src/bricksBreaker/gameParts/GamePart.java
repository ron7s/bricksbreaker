package bricksBreaker.gameParts;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import bricksBreaker.Board;

public abstract class GamePart extends Thread {
	protected int x, y, delay = 20;
	protected int width = 30, height = 30, level;
	protected JPanel p;
	
	public GamePart(JPanel panel){
		this.p = panel;
		x = 0;
		y = 0;
		level = Board.level;
	}
	
	public void run(){
		try {
			sleep(delay);
		} catch (Exception e) {
			
		}
		synchronized (this) {
			if (Board.isPaused) {
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public synchronized void setPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void draw(Graphics g) {
		Font font = new Font("Lucida Calligraphy", Font.BOLD, 20);
		g.setColor(Color.red);
		g.setFont(font);
		g.drawString("UNDEFINED", x, y);
	}
	
	public void kill() {
		//empty most of the parts don't need it because they kill themself, but some parts do need it
	}
	
	public ArrayList<String> getSaveData() {
		//this function purpose is to get the data that need to be saved from some parts
		ArrayList<String> data = new ArrayList<>();
		data.add("x=" + x);
		data.add("y=" + y);
		data.add("width=" + width);
		data.add("height=" + height);
		return data;
	}
	public void loadDataFromSave(HashMap<String, String> data) {
		//this function purpose is to set the data that need to be loaded to some parts
		x = Integer.parseInt(data.get("x"));
		y = Integer.parseInt(data.get("y"));
		width = Integer.parseInt(data.get("width"));
		height = Integer.parseInt(data.get("height"));
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setSize(int width, int height) {
		setWidth(width);
		setHeight(height);
	}
}
