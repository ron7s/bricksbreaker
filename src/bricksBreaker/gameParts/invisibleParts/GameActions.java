package bricksBreaker.gameParts.invisibleParts;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import bricksBreaker.Board;
import bricksBreaker.gameParts.GamePart;
import bricksBreaker.gameParts.collidableParts.Brick;

public class GameActions extends InvisiblePart {
	int timePassed = 0;
	public static boolean isCheatsMode = false;
	ArrayList<GamePart> gmprts = new ArrayList<>();
	HashMap<String, Integer> timeNames = new HashMap<>();// dictionary of:
	// name - lastCalled:(timePassed - lastCalled) = time need to pass in order to
	// do this

	public GameActions(JPanel panel) {
		super(panel);
	}

	public void run() {
		while (Board.lifes > 0 && level == Board.level) {
			super.run();
			timePassed += delay;
			String key = "fireModeActive";
			if (Brick.isFireMode && (!timeNames.containsKey(key) || isCheatsMode)) {
				timeNames.put(key, timePassed);
			} else if (timeNames.containsKey(key)) {
				if ((timePassed - timeNames.get(key)) >= 10000) {
					Brick.isFireMode = false;
					timeNames.remove(key);
				}
			}
			key = "suspendPaddle";
			if(Board.isPaddleSuspended && !(timeNames.containsKey(key)))
			{
				timeNames.put(key, timePassed);
			} else if (timeNames.containsKey(key)){
				if((timePassed - timeNames.get(key)) >= 320) {
					Board.isPaddleSuspended = false;
					timeNames.remove(key);
				}
			}
			
			//clearOldParts();
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public void clearOldParts()
	{
		ArrayList<GamePart> tmp;
		synchronized (((Board) p).getGameParts()) {
			tmp = (ArrayList<GamePart>) ((Board) p).getGameParts().clone();
		}
		for (GamePart gp : tmp) {
			if (!gp.isAlive()) {
				gmprts.add(gp);
			}
		}
		synchronized (((Board) p).getGameParts()) {
			for (GamePart gp : gmprts) {
				((Board) p).getGameParts().remove(gp);
			}
		}
		gmprts.clear();
		
		synchronized (((Board) p).getShortTermGameParts()) {
			tmp = (ArrayList<GamePart>) ((Board) p).getShortTermGameParts().clone();
		}
		for (GamePart gp : tmp) {
			if (!gp.isAlive()) {
				gmprts.add(gp);
			}
		}
		synchronized (((Board) p).getShortTermGameParts()) {
			for (GamePart gp : gmprts) {
				((Board) p).getShortTermGameParts().remove(gp);
			}
		}
		gmprts.clear();
		
		synchronized (((Board) p).getBricks()) {
			tmp = (ArrayList<GamePart>) ((Board) p).getBricks().clone();
		}
		for (GamePart brk : tmp) {
			if (!brk.isAlive()) {
				gmprts.add(brk);
			}
		}
		synchronized (((Board) p).getBricks()) {
			for (GamePart brk : gmprts) {
				((Board) p).getBricks().remove(brk);
			}
		}
		gmprts.clear();
		
	}
	
	public ArrayList<String> getSaveData() {
		ArrayList<String> data = new ArrayList<>();
		data.add("GameActions{");
		data.add("timePassed=" + timePassed);
		data.add("isCheatsMode=" + isCheatsMode);
		for (String option : timeNames.keySet()) {
			data.add("timeNames_" + option + "=" + timeNames.get(option));
		}
		
		ArrayList<String> tmp = super.getSaveData();
		for(String info : tmp)
		{
			data.add(info);
		}
		data.add("}");
		return data;
	}
	
	public void loadDataFromSave(HashMap<String, String> data) {
		super.loadDataFromSave(data);
		timePassed = Integer.parseInt(data.get("timePassed"));
		isCheatsMode = Boolean.parseBoolean(data.get("isCheatsMode"));
		for (String option : data.keySet()) {
			if(option.contains("timeNames")) {
				timeNames.put(option.substring(option.indexOf('_') + 1), Integer.parseInt(data.get(option)));
			}
		}
	}
}
