package bricksBreaker.gameParts.invisibleParts;

import java.awt.Graphics;

import javax.swing.JPanel;

import bricksBreaker.gameParts.GamePart;

public class InvisiblePart extends GamePart {

	public InvisiblePart(JPanel panel) {
		super(panel);
	}
	
	public void draw(Graphics g) {
		//empty action the purpose is to override the default draw because this kind of parts should be invisible
	}
}
