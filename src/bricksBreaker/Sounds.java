package bricksBreaker;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Sounds {
	public static void playSound(String name) {
		String path = "sounds/" + name + ".wav";
		try {
	        Clip clip = AudioSystem.getClip();
	        File sound = new File(path);
	        AudioInputStream inputStream = AudioSystem.getAudioInputStream(sound);
	        clip.open(inputStream);
	        clip.start();
	      } catch (Exception e) {
	        System.err.println(e.getMessage());
	      }
	}
}
