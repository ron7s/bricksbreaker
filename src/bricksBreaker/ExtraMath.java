package bricksBreaker;

import java.awt.Point;

public class ExtraMath {

	public static double getDistance(double x1, double y1, double x2, double y2) {
		return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
	}

	public static double sumDistanceFromTwoPoints(int sX1, int sY1, int sX2, int sY2, int destX, int destY) {
		return getDistance(sX1, sY1, destX, destY) + getDistance(sX2, sY2, destX, destY);
	}

	public static Point getNewSpeeds(int dx, int dy, double angle) {
		double remaining = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
		if(remaining == 0) return new Point(0, 0);
		double collisionAngle = (Math.PI / 2) - Math.acos(dy / remaining) - angle;
		double rotationAngle = Math.PI - (2 * collisionAngle);
		
		dy *= -1;
		
		double cos = (float)Math.cos(rotationAngle);
		double sin = (float)Math.sin(rotationAngle);

		int newX = (int)Math.round(dx * cos - dy * sin);
		int newY = (int)Math.round(dx * sin + dy * cos);
		
		return new Point(newX, newY);
	}
	
	public static Point getNewSpeeds(Vector ball, Vector normal) {
		Vector newVec = ball.sub(normal.scalarMultiplication(ball.scalarMultiplication(2).dot(normal) / (Math.pow(normal.getSize(), 2))));
		
		double bdn = ball.dot(normal);
		double m = (normal.getY() / normal.getX());
		double t = ball.getY() - (ball.getX() * m);
		double x = (bdn + normal.getY() * t) / (Math.pow(normal.getY(), 2) / normal.getX() + normal.getX());
		double y = m * x - t;
		
		System.out.println("cur:" + ball.getX() + ", " + ball.getY() + " new: " +
				(int)Math.round((int)Math.round(x)) + ", " + (int)Math.round((int)Math.round(y)));
		System.out.println("t: " + t + "  bdn: " + bdn + "  m:" + m + "  x: " + x + "  y:" + y);
		return new Point((int)Math.round(x), (int)Math.round(y));
	}

}
